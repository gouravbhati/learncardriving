import React from "react";
import { AppRegistry } from "react-native";
import App from "./App";
import { name as appName } from "./app.json";
import { Root } from "native-base";
import bgMessaging from './bgMessaging';
import { Provider } from 'react-redux'
import { createStore } from 'redux';
import reducers from './src/store/reducer';
const RootApp = () => (
  <Provider store={createStore(reducers)}>
    <App/>
  </Provider>
);

AppRegistry.registerComponent(appName, () => RootApp);
AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => bgMessaging); 