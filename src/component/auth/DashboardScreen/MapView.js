import React, { Component } from 'react';
import { StyleSheet, PermissionsAndroid } from 'react-native';
import { connect } from 'react-redux';
import MapView, { Marker } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import customeMapStyle from '../../utility/MapStyle.json';

class MapViewScreen extends Component {
    state = {
        latitude: '',
        longitude: '',
    }

    componentDidMount = async () => {
        this.getLocationPermmision();
        this.renderInstructures(this.props.premium_instructor);
        await Geolocation.getCurrentPosition(info => {
            this.setState({ latitude: parseFloat(info.coords.latitude) });
            this.setState({ longitude: parseFloat(info.coords.longitude) });
        },
            (error) => { this.setState({ latitude: 23.554, longitude: 54.56546 }) }),
            { enableHighAccuracy: true, timeout: 10000, maximumAge: 3000 }
    }

    getLocationPermmision = () => {
        try {
            PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
        } catch (err) {
        }
    }

    renderInstructures = (list) => {
        let markers = [];
        for (var i = 0; i < list.length; i++) {
            markers.push(
                <Marker
                    coordinate={{ latitude: list[i].lat, longitude: list[i].lng }}
                    title={list[i].name}
                    image={require('../../../assets/marker.png')}
                />
            );
        }
        return markers
    }


    render() {
        return (
            <MapView style={{ flex: 1 }}
                customMapStyle={customeMapStyle}
                initialRegion={{
                    latitude: this.state.latitude,
                    longitude: this.state.longitude,
                    latitudeDelta: 0.03,
                    longitudeDelta: 0.03
                }}
            >
                <Marker
                    coordinate={{ latitude: this.state.latitude, longitude: this.state.longitude }}
                    title={"Your Current Location"}
                    image={require('../../../assets/geo_loc.png')}
                />
                {this.renderInstructures(this.props.premium_instructor).map((value) => value)}
                {this.renderInstructures(this.props.normal_instructor).map((value) => value)}
            </MapView>
        );
    }
}

const mapStateToProp = state => {
    return {
        lang: state.staticLang.userLanguage,
        slider: state.rootReducer.sliders,
        premium_instructor: state.rootReducer.premium_instructor.slice(0, 5),
        normal_instructor: state.rootReducer.normal_instructor.slice(0, 5),
    }
}

export default connect(mapStateToProp)(MapViewScreen);
