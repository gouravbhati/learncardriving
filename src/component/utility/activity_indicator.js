import React from 'react';
import {Modal, View, ActivityIndicator} from 'react-native';
function renderActivity({show}){
    if (show)
        return (
            <Modal transparent={true} >
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                    <ActivityIndicator size='large' color='yellow' />
                </View>
            </Modal>
        )
    return null
}


export default renderActivity