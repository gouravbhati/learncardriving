import React from 'react';
import { Item, Input, View, Label, DatePicker } from 'native-base'
import { radius, margin, colors, padding, fonts } from '../../styles/baseStyle';


const AppInput = ({
    placeholder,
    keyboardType,
    onKeyPress,
    onChangeText,
    errorMsg,
    value,
    label,
    inputStyle,
    secureTextEntry,
    placeHolderText,
    minimumDate,
    maximumDate,
}) => {
    return (
        <View>
        <Label style={Styles.floatingStyle}>{label}</Label>
        <View style={{ borderColor: 'white', borderWidth: 1, borderRadius: 9, paddingTop: 5 }} >
           
            <DatePicker
                locale={"en"}
                timeZoneOffsetInMinutes={undefined}
                minimumDate={minimumDate}
                maximumDate={maximumDate}
                modalTransparent={true}
                animationType={"fade"}
                androidMode={"default"}
                placeHolderText={placeHolderText}
                textStyle={{ color: "white", fontSize: 15 }}
                placeHolderTextStyle={{ color: "rgba(255,255,255,0.5)" }}
                onDateChange={onChangeText}
            />
        </View>
        </View>
    );
}
const Styles = {
    inputStyle: {
        borderRadius: radius.xl,
        marginVertical: margin.md,
        height: 55,
        alignSelf: 'center',
    },
    textStyle: {
     
        color: colors.white,
      
    },
    floatingStyle: {
        color: colors.white,
       marginBottom: margin.sm

    }
}

{/* <View>

            <Item regular floatingLabel style={[Styles.inputStyle, inputStyle]}>
                <Label style={Styles.floatingStyle}>{label}</Label>
                <DatePicker
                                    locale={"en"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={true}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    placeHolderText="Select date"
                                    textStyle={{ color: "white", fontSize: 16 }}
                                    placeHolderTextStyle={{ color: "#d3d3d3" }}
                                    onDateChange={(date) => this.updateDate(date)}
                                />
            </Item>

        </View> */}


export default AppInput;