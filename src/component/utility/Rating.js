import React from 'react';
import Icon from 'native-base';


export default function Rating() {
    return (
        <Icon name="star" style={{ color: 'yellow' }} />
    )
}