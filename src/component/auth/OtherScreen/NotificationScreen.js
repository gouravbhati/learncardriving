import React, { Component } from 'react';
import { FlatList, ScrollView, Button, TouchableOpacity, Image } from 'react-native'
import { Card, View, Text } from 'native-base'
import { baseStyles, colors, padding, radius, fonts, margin } from '../../../styles/baseStyle';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import PTRView from 'react-native-pull-to-refresh';
import { Remove_Notification, Add_Notification} from '../../../store/reducer/actions/notification';
class NotificationScreen extends Component {
    state = {
        d: [],
        refresh: '',
    }

    async componentDidMount() {
        console.log(this.props.data);
        await this.setState({ d: this.props.data })

    }
   
    _refresh = () => {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve();
                this.setState({ refresh: '' });
            }, 2000)
        });
    }

    render() {
        return (
            <>
                <PTRView onRefresh={this._refresh}>
                    <View style={baseStyles.appContainer}>

                        <ScrollView>
                            <View>
                                {
                                    this.state.d.length > 0 ?
                                        this.state.d.map((item, k) => {
                                            console.log(item.title1)
                            
                                            return (
                                                <Card style={Styles.cardStyle} key={k}>
                                                    <Text style={Styles.heading}>{item.title1}</Text>
                                                    <Text style={Styles.subHeading}>{item.body1}</Text>
                                                    <Text style={Styles.subHeading}>{item.date}</Text>
                                                </Card>
                                            )
                                        })
                                        :

                                        <Card style={Styles.cardStyle} >
                                           
                                            <Text style={Styles.notification1}>{this.props.lang.no_notification_found}</Text>
                                        </Card>

                                }
                            </View>
                            {/* <TouchableOpacity style={Styles.reload} onPress={() => this.setState({ a: '' })}>
                            <View style={{ flexDirection: 'row', alignSelf: 'center' }}>
                                <Image style={{ alignSelf: 'center' }} source={require('../../../assets/refresh.png')} />
                                <Text style={Styles.notification}>Reload</Text>
                            </View>
                            <Text style={Styles.notification1}>To View New Notifications</Text>
                        </TouchableOpacity> */}
                        </ScrollView>
                    </View>
                </PTRView>
                <View style={{backgroundColor: 'black'}}>
                    <Text style={Styles.notification}>{this.props.lang.scroll_down_to_refresh}</Text>
                </View>
            </>
        );

    }
}
const Styles = {
    cardStyle: {
        backgroundColor: colors.offBlack,
        padding: padding.lg,
        borderColor: 0,
        borderRadius: radius.lg
    },
    heading: {
        color: colors.white,
        fontWeight: 'bold',
        fontSize: fonts.xl,
        marginBottom: margin.md
    },
    subHeading: {
        color: colors.gray,
        fontSize: fonts.mld,
        marginBottom: margin.md
    },
    notification: {
        textAlign: 'center',
        color: '#555a66',
        fontSize: 20,
        marginBottom: 30,

    },
    notification1: {
        textAlign: 'center',
        color: '#555a66',
        fontSize: 20
    },
    reload: {
        flex: 0.5,

    }
}

const mapStateToProps = (state) => {
    console.log(state.notification.notification);
    return {
        data: state.notification.notification,
        lang: state.staticLang.userLanguage
    }
}

export default connect(mapStateToProps, {Remove_Notification, Add_Notification})(NotificationScreen);

