
export const cards = [
    {
        image: require('../../../assets/sw_img-1.png'),
    },
    {
        image: require('../../../assets/sw_img-1.png'),
    },
    {
        image: require('../../../assets/sw_img-1.png'),
    },
];

export const celebraty = [
    {
        src: require('../../../assets/user_2.png'),
        name:'mohammad',
        experience:'8 Yrs of Experience',
        address:'South Avenue,Manama, Bahrain ',
        certificate:'Gov. Licence Trainer ',
        rating: require('../../../assets/rating.png'),

    },
    
];

export const traneeList = [
    {
        src: require('../../../assets/user_2.png'),
        name:'mohammad',
        experience:'8 Yrs of Experience',
        address:'South Avenue,Manama, Bahrain ',
        certificate:'Gov. Licence Trainer ',
        rating: require('../../../assets/rating.png'),

    },
    {
        src: require('../../../assets/user_2.png'),
        name:'mohammad',
        experience:'8 Yrs of Experience',
        address:'South Avenue,Manama, Bahrain ',
        certificate:'Gov. Licence Trainer ',
        rating: require('../../../assets/rating.png'),

    },
    {
        src: require('../../../assets/user_2.png'),
        name:'mohammad',
        experience:'8 Yrs of Experience',
        address:'South Avenue,Manama, Bahrain ',
        certificate:'Gov. Licence Trainer ',
        rating: require('../../../assets/rating.png'),

    },
    {
        src: require('../../../assets/user_2.png'),
        name:'mohammad',
        experience:'8 Yrs of Experience',
        address:'South Avenue,Manama, Bahrain ',
        certificate:'Gov. Licence Trainer ',
        rating: require('../../../assets/rating.png'),

    },
    {
        src: require('../../../assets/user_2.png'),
        name:'mohammad',
        experience:'8 Yrs of Experience',
        address:'South Avenue,Manama, Bahrain ',
        certificate:'Gov. Licence Trainer ',
        rating: require('../../../assets/rating.png'),

    },
    {
        src: require('../../../assets/user_2.png'),
        name:'mohammad',
        experience:'8 Yrs of Experience',
        address:'South Avenue,Manama, Bahrain ',
        certificate:'Gov. Licence Trainer ',
        rating: require('../../../assets/rating.png'),

    },
    {
        src: require('../../../assets/user_2.png'),
        name:'mohammad',
        experience:'8 Yrs of Experience',
        address:'South Avenue,Manama, Bahrain ',
        certificate:'Gov. Licence Trainer ',
        rating: require('../../../assets/rating.png'),

    },
    
];

export const normalTrainee = [
    {
        src: require('../../../assets/user_3.png'),
        name:'salim',
        experience:'8 Yrs of Experience',
       },
       {
        src: require('../../../assets/user_2.png'),
        name:'aslam',
        experience:'8 Yrs of Experience',
       },
       {
        src: require('../../../assets/user_3.png'),
        name:'mohammad',
        experience:'8 Yrs of Experience',
       },
       {
        src: require('../../../assets/user_2.png'),
        name:'mohammad',
        experience:'8 Yrs of Experience',
       },
    
];

export const location = [
    {
        title: "Will Go to Trainers Location \n Rd No 2151, Manama, Bahrain",
    },
    {
        title: "Trainer will come to my Address \n Trainer will come to my Address",
    },
];

export const locationChange = [
    {
        title: "Will Go to Trainers Location \n Rd No 2151, Manama, Bahrain",
    },
   
];

export const credit = [
    {
        title: "Enter Your Credit Card Details",
    },
   
];
export const paypal = [
    {
        title: "Enter Your Paypal Details",
    },
   
];
export const instructor = 
    {
        name: "Mohammad",
        profile:require('../../../assets/user_2.png'),
        Member_Since: "22 Dec 2018",
        experience: "5 years",
        hours: "220",
        ongoing: "2",
        location: "Manama, Bahrain",
        instructor_Detail: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor",
        vehicle: "Automatic Car",
        vehicle_Make: "Hyundai Sonata ",
        vehicle_Model: "HYUNDAI",
        vehicle_Year_Make: "2016",
        license_Category: "Govt. License Trainer"

    }


    export const ratingReview = [
        {
            image: require('../../../assets/user_3.png'),
            rating_star: require('../../../assets/rating.png'),
            name:'John Doe',
            description:'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
           },
           {
            image: require('../../../assets/user_2.png'),
            rating_star: require('../../../assets/rating.png'),
            name:'John Doe',
            description:'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.',
           },
        ];

        export const calList = [
            {
                day: "Monday",
                date: "1 Nov 2019",
                time: "11:00 - 12:00pm",
            },
            {
                day: "Tuesday",
                date: "2 Nov 2019",
                time: "10:00 - 11:00pm",
            },
            {
                day: "Wednesday",
                date: "5 Nov 2019",
                time: "8:00 - 10:00pm",
            },
            {
                day: "Thursday",
                date: "8 Nov 2019",
                time: "11:00 - 12:00pm",
            },
        ]
