import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, FlatList, Text, ScrollView, TouchableOpacity } from 'react-native';
import { baseStyles, margin, colors, fonts, radius } from '../../../styles/baseStyle';
import { Thumbnail, ListItem } from 'native-base';
import { Instructor_detail } from '../../../store/reducer/actions/instructor_detail';
import AsyncStorage from '@react-native-community/async-storage';
import rating from '../../utility/StarRating';
import Activity from '../../utility/activity_indicator';

class TraneeList extends Component {
    state = {
        trainers: '',
        token: '',
        activity: false
    }
    async componentDidMount() {
        let type = this.props.navigation.getParam('type');

        let token1 = await AsyncStorage.getItem('TOKEN');
        this.setState({ token: token1 });

        if (type == 'PRIME') {
            this.setState({ trainers: this.props.premium_instructor });
        }
        else if (type === 'search') {
            this.setState({ trainers: this.props.seach_data })
        }
        else {
            this.setState({ trainers: this.props.normal_instructor });
        }
    }

    callApi = async (item) => {
        this.setState({ activity: true })
        let res = await fetch("http://168.235.81.165/cardrive/public/api/user/instructor-details/" + item.id, {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${this.state.token}`,
                'Content-Type': 'application/json'
            }
        });
        let result = await res.json();
        this.setState({ activity: false })
        if (result.success === true) {
            this.props.Instructor_detail(result.data);
            this.props.navigation.navigate('instructorDetail', { rating: item.average_rating })
        }
    }
    render() {
        return (
            <View style={baseStyles.appContainer}>
                <ScrollView>
                    <Activity show={this.state.activity} />
                    <FlatList
                        data={this.state.trainers}
                        renderItem={({ item }) =>
                            <ListItem >

                                <TouchableOpacity style={[baseStyles.media, baseStyles.marginVerticalMd]}
                                    onPress={() => {
                                        this.callApi(item);
                                    }} >
                                    <Thumbnail source={{ uri: item.profile_url }} style={Styles.profile} />
                                    <View style={baseStyles.mediaBody}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: margin.sm }}>
                                            <Text style={Styles.heading}>{item.name}</Text>
                                            <Thumbnail source={item.rating} style={Styles.rating} />
                                        </View>
                                        <Text style={Styles.text}>{item.year_of_experience} Yrs of Experience</Text>
                                        <Text style={Styles.text}> {item.city},{item.state} </Text>
                                        <Text style={Styles.text}> {item.licence_type} Licence Trainer </Text>
                                        <View style={{ flexDirection: 'row' }} >
                                            {rating(parseInt(item.average_rating)).map(value => value)}
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </ListItem>


                        }
                    />
                </ScrollView>
            </View>
        );
    }
}

const Styles = {
    profile: {
        height: 70,
        width: 70,
        borderRadius: radius.lg,
        marginRight: margin.md
    },
    text: {
        color: colors.white,
        fontSize: fonts.md,
    },
    rating: {
        width: 100,
        height: 20,
        resizeMode: 'contain'
    },
    heading: {
        fontWeight: 'bold',
        fontSize: fonts.xl,
        color: colors.white
    }
}
const mapStateToProp = state => {
    return {
        premium_instructor: state.rootReducer.premium_instructor,
        normal_instructor: state.rootReducer.normal_instructor,
        seach_data: state.search_instructor.data,
    }
}

export default connect(mapStateToProp, { Instructor_detail })(TraneeList);