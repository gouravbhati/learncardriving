import React, { Component } from 'react';
import { Modal, Text, TouchableHighlight, View, Alert } from 'react-native';
import { colors, radius } from '../../styles/baseStyle';

export default class AppModal extends Component {
    state = {
        modalVisible: false,
    };

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    render() {
        return (
            <View style={{position:'relative'}}>
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        Alert.alert('Modal has been closed.');
                    }}>
                    <View>
                        <View style={Styles.modalDialog}>
                            <Text style={{ color: '#fff' }}>Hello World!</Text>

                            <TouchableHighlight
                                onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible);
                                }}>
                                <Text style={{ color: '#fff' }}>Hide Modal</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

const Styles = {
    modalDialog: {
        position: 'absolute',
        backgroundColor:rgba(0, 0, 0, 0.3),
        width:'100%',
        justifyContent:'center',
        alignItems:'center',
        top:200,
        backgroundColor:colors.white,
        width:300,
        height:500,
        borderRadius:radius.lg

       
    }
}