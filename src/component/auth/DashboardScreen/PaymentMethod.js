import React, { Component } from 'react';
import { FlatList } from 'react-native';
import { Segment, Text, View, Item, Input, Icon } from 'native-base';
import { colors, baseStyles, radius, padding, fonts, margin } from '../../../styles/baseStyle';
import AppIconButton from '../../utility/AppIconButton';
import AppButton from '../../utility/AppButton';
import AppRadioButton from '../../utility/AppRadioButton';
import AppInput from '../../utility/AppInput';
import { paypal, credit } from '../DashboardScreen/data';

export default class PaymentMethod extends Component {
    state = {
        activePage: 1,
    }

    selectComponent = (activePage) => () => this.setState({ activePage })

    _renderComponent = () => {
        if (this.state.activePage === 1)
            return <FlatList
                data={credit}
                renderItem={({ item }) =>
                    <View style={baseStyles.marginTopXl}>
                        <Text style={Styles.heading}>{item.title}</Text>
                        <AppInput label="Enter Card number" />
                        <AppInput label="Expiry Date" />
                        <AppInput label="Security Code" />
                    </View>}
            />
        else
            return <FlatList
                data={paypal}
                renderItem={({ item }) =>
                    <View style={baseStyles.marginTopXl}>
                        <Text style={Styles.heading}>{item.title}</Text>
                        <AppInput label="Enter Card number" />
                        <AppInput label="Expiry Date" />
                        <AppInput label="Security Code" />
                    </View>}
            />
    }

    render() {
        return (
            <View style={[baseStyles.appContainer, baseStyles.justifyContentStart]}>

                <View style={Styles.sagmentStyle}>
                    <AppIconButton active={this.state.activePage === 1}
                        onPress={this.selectComponent(1)}
                        btnStyle={{ backgroundColor: this.state.activePage === 1 ? "#FFE400" : "#000", marginRight: margin.lg, marginVertical: margin.md,
                        borderColor: this.state.activePage === 1 ? "#000" : "#C5C5C5", borderWidth: 1 }}
                        image={require('../../../assets/credit.png')}
                        titleStyle={{ color: this.state.activePage === 1 ? "#000" : "#fff" }}
                        title="Credit / Debit Card"
                    />

                    <AppIconButton active={this.state.activePage === 2}
                        onPress={this.selectComponent(2)}
                        titleStyle={{ color: this.state.activePage === 2 ? "#000" : "#fff" }}
                        btnStyle={{
                            backgroundColor: this.state.activePage === 2 ? "#FFE400" : "#000", marginRight: margin.lg, marginVertical: margin.md,
                            borderColor: this.state.activePage === 2 ? "#000" : "#C5C5C5", borderWidth: 1
                        }}
                        image={require('../../../assets/paypal_2.png')}
                        title="Paypal" />

                </View>
                <View padder>
                    {this._renderComponent()}
                </View>


                <View style={{ justifyContent: 'flex-end', flex: 1 }}>
                    <AppButton
                        title="Continue to Pay"
                        btnStyle={{
                            borderRadius: radius.xxl,
                            borderTopLeftRadius: 0,
                            paddingHorizontal: padding.xxl
                        }}
                        onPress={() => this.props.navigation.navigate('sucessScreen')}
                    />
                </View>
            </View>
        )
    }
}

const Styles = {

    sagmentStyle: {
        justifyContent: 'center',
        flexDirection: 'column',
        backgroundColor: colors.black,
    },
    heading: {
        fontSize: fonts.lg,
        fontWeight: 'bold',
        color: colors.white,
        marginBottom: margin.lg
    }
}

