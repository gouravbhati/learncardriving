import React from 'react';
import {Image} from 'react-native'
import { Button, Text, View, Icon,} from 'native-base'
import { colors,fonts, margin } from '../../styles/baseStyle';

const AppIconButton = ({title,btnStyle,titleStyle,onPress,active,image,imgStyle}) => {
    return (
        <View>
            <Button
                style={[Styles.btnStyle,btnStyle]}
                onPress={onPress}
                active={active}
            >
                <Image source={image} style={[Styles.imgStyle,imgStyle]}/>
                <Text style={[Styles.titleStyle,titleStyle]}>{title}</Text>
            </Button>
        </View>
    );
}

const Styles = {
    btnStyle: {
        backgroundColor: colors.yellow,
        flexDirection: 'row',
        justifyContent: 'center',
        textAlign: 'center',
        marginVertical:margin.xl,
        height:55
    },
    titleStyle:{
        color:colors.black,
        fontSize:fonts.xl,
    
    },
    imgStyle:{
        width:20,
        height:20,
        resizeMode:'contain'
    }
}
export default AppIconButton;