import { search } from '../api utility/apis';

export default fetchUserDataByAPI = async (name, lat, long, lic, gender, token, lang) => {
    let result = await fetch(`${search}lat=${lat}&lng=${long}&distance=100&name${name}=&licence_type=${lic}&gender=${gender}`, {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${token}`,
            'X-localization': lang
        }
    })
    return await result.json();
}
