export default fetchUserDataByAPI = async (path, token, lang) => {
    let result = await fetch(path, {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${token}`,
            'X-localization': lang
        }
    })
    return await result.json();
}
