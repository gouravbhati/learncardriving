export const setSlider = slides => {
    return {
        type: 'SET_SLIDER',
        payload: slides
    }
}

export const setPrimeInstructure = prime => {
    return {
        type: 'SET_PRIMIUM_INSTRUCTOR',
        payload: prime
    }
}

export const setNormalInstructure = normal => {
    return {
        type: 'SET_NORMAL_INSTRUCTOR',
        payload: normal
    }
}

export const setBookedInstructure = normal => {
    return {
        type: 'SET_BOOKED_INSTRUCTOR',
        payload: normal
    }
}



