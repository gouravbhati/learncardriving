import React, { useState } from 'react';
import { View, CheckBox, Text, } from 'native-base';
import { radius, colors, margin } from '../../styles/baseStyle';

export default AppCheckBox = ({ value, title, checkContainer, titleStyle, checkBoxStyle, onPress }) => {
    //const [value, setValue] = useState(false);
    return (

        <View style={[Styles.checkContainer, checkContainer]}>
            <CheckBox
                color="#000"
                selectedColor="#eee"
                style={[Styles.checkBoxStyle, checkBoxStyle]}
                onPress={onPress}
                checked={value}
            />
            <Text style={[Styles.titleStyle, titleStyle]}>{title}</Text>
        </View>

    );
}

const Styles = {
    checkBoxStyle: {
        borderRadius: radius.sm,
        height: 25,
        width: 25,
        borderColor: colors.white,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 4
    },
    titleStyle: {
        marginLeft: margin.lg,
        color: colors.white
    },
    checkContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: margin.md
    }

}