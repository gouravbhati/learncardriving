import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { baseStyles,radius,padding } from '../../../styles/baseStyle';
import AppButton from '../../utility/AppButton';


export default class SucessTraning extends Component {
    render() {
        return (
            <View style={[baseStyles.appContainer]}>
                <Image
                    source={require('../../../assets/sucess_t.png')}
                    style={{ width: 300, height: null,flex:1, resizeMode: 'contain',alignSelf:'center' }}
                />
                
                <AppButton 
                title="Continue" 
                btnStyle={{
                    borderRadius: radius.xxl,
                    borderTopLeftRadius: 0,
                    paddingHorizontal: padding.xxl
                }}
                onPress={() => this.props.navigation.navigate('dashboard')}/>
            </View>
        );
    }
}


