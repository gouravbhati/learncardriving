import React, { Component } from 'react';
import { Dimensions, View, PermissionsAndroid } from 'react-native';
import { connect } from 'react-redux';
import FastImage from 'react-native-fast-image';
import { colors } from '../styles/baseStyle';
import AsyncStorage from '@react-native-community/async-storage';
import { setStaticLangArabic, setStaticLangEnglish } from '.././store/reducer/actions/staticLanguageReduceraction';
import { dashboard, booked_instructur } from '../component/utility/api utility/apis';
import fetchUserData from '../component/utility/api utility/fetchuserdatabyapi';
import { setSlider, setNormalInstructure, setPrimeInstructure, setBookedInstructure } from '.././store/reducer/actions/rootReducerAction';

/* 
All App state code goes here
*/

class SplashScreen extends Component {
  componentDidMount = async () => {
    this.getLocationPermmision();
    let lang = await AsyncStorage.getItem('LANG');
    if (lang == 'ar') {
      this.props.setStaticLangArabic();
    }
    else {
      this.props.setStaticLangEnglish();
    }
    let status = await AsyncStorage.getItem('STATUS');
    this._navigateToUserByStatus(status);
  }

  _navigateToUserByStatus = async (status) => {
    switch (status) {
      case 'LOGEDIN':
        let response = await fetchUserData(dashboard, await AsyncStorage.getItem('TOKEN'), 'en');
        let booked_instructure = await fetchUserData(booked_instructur, await AsyncStorage.getItem('TOKEN'), 'en');
        if (response.success) {
          this.props.setSlider(response.data.sliders);
          this.props.setBookedInstructure(booked_instructure.data);
          this.props.setNormalInstructure(response.data.normal_instructor);
          this.props.setPrimeInstructure(response.data.premium_instructor);
          this.props.navigation.navigate('dashboard');
          return 0
        }
        this.props.navigation.navigate('home');
        return 0
      default:
        this.props.navigation.navigate('home');
        return 0
    }
  }

  getLocationPermmision = () => {
    try {
      PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
    } catch (err) {
    }
  }


  render() {
    return (
      <View style={{
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
        backgroundColor: colors.black,
        justifyContent: 'center',
        alignItems: 'center'
      }}>
        <FastImage
          style={{
            width: 200,
            height: 300,
            resizeMode: 'contain'
          }}
          source={require('../assets/splash.png')}
        />
      </View>
    )
  }
}


const mapStateToProp = state => {
  return {
    lang: state.staticLang.userLanguage
  }
}

export default connect(mapStateToProp, { setBookedInstructure, setSlider, setNormalInstructure, setPrimeInstructure, setStaticLangArabic, setStaticLangEnglish })(SplashScreen);
