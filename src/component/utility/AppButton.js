import React from 'react';
import { Button, Text, View, } from 'native-base'
import { colors, fonts, margin } from '../../styles/baseStyle';

const AppButton = ({ title, btnStyle, titleStyle, onPress, active }) => {
    return (
        <View>
            <Button
                style={[Styles.btnStyle, btnStyle]}
                onPress={onPress}
                active={active}
            >
                <Text uppercase={false} style={[Styles.titleStyle, titleStyle]}>{title}</Text>
            </Button>
        </View>
    );
}

const Styles = {
    btnStyle: {
        backgroundColor: colors.yellow,
        flexDirection: 'row',
        justifyContent: 'center',
        textAlign: 'center',
        marginVertical: margin.xl,
        height: 55
    },
    titleStyle: {
        color: colors.black,
        fontSize: fonts.lg,

    }
}
export default AppButton;