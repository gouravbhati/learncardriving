import React from 'react'
import { Image, Button, TouchableOpacity } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation';
import AppAvatar from '../../utility/AppAvatar';
import { colors, fonts } from '../../../styles/baseStyle';
import { Icon, Thumbnail } from 'native-base';
import MainDashboard from './MainDashboard';
import SearchScreen from '../DashboardScreen/SearchScreen';
import InstructorDetail from './InstructorDetail';
import BookInstructor from './BookInstructor';
import TraneeList from './TraneeList';
import ReviewScreen from './ReviewScreen ';
import PaymentMethod from './PaymentMethod';
import LearnRoot from '../LearnScreen';
import NotificationScreen from '../OtherScreen/NotificationScreen';
import PaySucess from '../DashboardScreen/paySucess';
import MapView from '../DashboardScreen/MapView';
import HeaderTitle from '../../../static language/staticlangHeader';

const RootDashboardNavigation = createStackNavigator({

    appNotify: {
        screen: NotificationScreen,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />),
            headerTitle: <HeaderTitle title={'Notification'} />,
            headerTitleStyle: { color: colors.white, fontSize: fonts.xl, fontWeight: 'bold' },
        }
    },

    dashboard: {
        screen: MainDashboard,
        navigationOptions: ({ navigation }) => ({
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerLeft: (() =>
                <TouchableOpacity onPress={() => (navigation.navigate('appNotify'))}>
                    <Image source={require('../../../assets/notification.png')} style={Styles.backImg} />
                </TouchableOpacity>
            ),
            headerTitle: <HeaderTitle title={'Explore'} />,
            headerTitleStyle: { color: colors.white, fontSize: fonts.xl, fontWeight: 'bold' },
            headerRight: (() => (
                <AppAvatar navigation={navigation} navigatetomap={true} />
            ))
        }),
    },

    mapview: {
        screen: MapView,
        navigationOptions: ({ navigation }) => ({
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerLeft: (() =>
                <TouchableOpacity onPress={() => (navigation.navigate('appNotify'))}>
                    <Image source={require('../../../assets/notification.png')} style={Styles.backImg} />
                </TouchableOpacity>
            ),
            headerTitle: <HeaderTitle title={'Explore'} />,
            headerTitleStyle: { color: colors.white, fontSize: fonts.xl, fontWeight: 'bold' },
            headerRight: (() => (
                <AppAvatar navigation={navigation} navigatetomap={false} />
            ))
        }),
    },
    search: {
        screen: SearchScreen,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />),
            headerTitle: <HeaderTitle title={'Search'} />,
            headerTitleStyle: { color: colors.white, fontSize: fonts.xl, fontWeight: 'bold' },

        }
    },
    instructorDetail: {
        screen: InstructorDetail,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />),
            headerTitle: <HeaderTitle title={'Instructor Detail'} />,
            headerTitleStyle: { color: colors.white, fontSize: fonts.xl, fontWeight: 'bold' },
        }
    },
    bookInstructor: {
        screen: BookInstructor,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />),
            headerTitle: <HeaderTitle title='Book Instructor' />,
            headerTitleStyle: { color: colors.white, fontSize: fonts.xl, fontWeight: 'bold' },
        }
    },
    traneeList: {
        screen: TraneeList,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />),
            headerTitle: <HeaderTitle title='All Trainers' />,
            headerTitleStyle: { color: colors.white, fontSize: fonts.xl, fontWeight: 'bold' },

        }
    },
    review: {
        screen: ReviewScreen,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />),
            headerTitle: <HeaderTitle title={'Review'} />,
            headerTitleStyle: { color: colors.white, fontSize: fonts.xl, fontWeight: 'bold' },
        }
    },
    paymentMethod: {
        screen: PaymentMethod,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />),
            headerTitle: <HeaderTitle title={'Payment Method'} />,
            headerTitleStyle: { color: colors.white, fontSize: fonts.xl, fontWeight: 'bold' },

        }
    },
    sucessScreen: {
        screen: PaySucess,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />),
            headerTitle: <HeaderTitle title={'Payment Method'} />,
            headerTitleStyle: { color: colors.white, fontSize: fonts.xl, fontWeight: 'bold' },

        }
    },
},
    {
        initialRouteName: "dashboard",
        headerBackTitleVisible: false,
        headerLayoutPreset: 'center'
    }
)

const DashboardRoot = createAppContainer(RootDashboardNavigation);


const TabRoot = createBottomTabNavigator({

    trainers: {
        screen: DashboardRoot,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/notification.png')} style={Styles.backImg} />),
            headerTitle: "Explore",
            tabBarLabel: <HeaderTitle title={'trainer'} />,
            tabBarIcon: (({ tintColor }) => (
                <Icon name='ios-person' style={{ color: tintColor, fontSize: 26 }} />
            )),
        }
    },
    learn: {
        screen: LearnRoot,
        navigationOptions: {
            tabBarLabel: <HeaderTitle title={'learn'} /> ,
            tabBarIcon: (({ tintColor }) => (
                <Icon name='radio-button-on' style={{ color: tintColor, fontSize: 26 }} />
            )),
        }
    },
},
    {
        tabBarOptions: {
            activeTintColor: '#FFE200',
            inactiveTintColor: '#fff',
            showIcon: true,
            showLabel: true,
            tintColor: '#fff',
            style: { backgroundColor: "#212121", borderTopColor: null, paddingTop: 5 },
            labelStyle: { fontSize: 14 },
            labelPosition: 'below-icon'
        },

        defaultNavigationOptions: ({ navigation }) => ({
            tabBarVisible: navigation.state.routes[navigation.state.index].routeName == 'dashboard'
                ||
                navigation.state.routes[navigation.state.index].routeName == 'learnDash'
                ||
                navigation.state.routes[navigation.state.index].routeName == 'mapview'
                ? true : false
        })
    });

const AppRoot = createAppContainer(TabRoot)

const Styles = {
    backImg: {
        width: 35,
        height: 20,
        resizeMode: 'contain'
    }
}
export default AppRoot;