import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { colors, fonts, padding, margin } from '../../styles/baseStyle';
import Slider from '@react-native-community/slider';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: this.props.value,
    };
  }

  change(value) {
    this.setState(() => {
      return {
        value: parseFloat(50),
      };
    });
  }

  render() {
    console.log(this.props)
    const { value } = this.state;
    return (
      <View style={[Styles.sliderContainer]}>
        {/* <Slider
          step={1}
          maximumValue={100}
          minimumValue={0}
          onValueChange={this.change.bind(this)}
          value={50}
          maximumTrackTintColor="#212121"
          minimumTrackTintColor="#FFE200"
          thumbTintColor="transparent"
          style={{ transform: [{ scaleY: 9.5 }] }}
        /> */}
        <View style={Styles.container}>
          <Text style={{ color: colors.white }}>Completed</Text>
          <Text style={Styles.text}>{String(value)}</Text>
        </View>
      </View>
    );
  }
}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  text: {
    fontSize: fonts.lg,
    color: colors.yellow
  },
  sliderContainer: {
    flex: 1,
    marginVertical: margin.lg,
    marginHorizontal: margin.lg
  }
});