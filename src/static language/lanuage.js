export const Arabic = {
    // Home Page
    Hello: 'مرحبا',
    message: 'مرحبا بكم في سائق - تطبيق العملاء. لمتابعة استخدام التطبيق لدينا يرجى تسجيل الدخول أو التسجيل',
    login: 'تسجيل الدخول',
    account: 'لا تملك حساب؟',
    signup: 'سجل',
    email: 'البريد الإلكتروني',

    // Signup
    first_name: 'الاسم الاول',
    last_name: 'الكنية',
    mobile: 'رقم الهاتف المحمول',
    cpr: 'رقم البطاقة السكانية',
    nationality: 'جنسية',
    dob: 'تاريخ الولادة',
    password: 'كلمه السر',
    confirm_password: 'تأكيد كلمة المرور',
    male: 'الذكر',
    female: 'أنثى',
    driving_lic_no: 'رقم رخصة القيادة',
    tem_lic_no: 'رقم رخصة القيادة المؤقتة',
    issue_date: 'تاريخ إصدار رخصة القيادة',
    expirary_date: 'تاريخ انتهاء رخصة القيادة',
    term: 'بالمتابعة أنت توافق على موقعنا ',
    condition: 'الأحكام والشروط',
    select_date: 'حدد تاريخ',

    //Login screen
    forgot_password: 'هل نسيت كلمة المرور؟',
    continue: 'استمر',
    reset_password: 'إعادة تعيين كلمة المرور',
    new_password: 'كلمة مرور جديدة',
    enter_otp: 'أدخل OTP',

    // welcome screen
    welcome_to: 'مرحبا بك في',
    driver_customer_app: 'سائق تطبيق العملاء',

    //dashboard 
    explore: 'يكتشف',
    search: 'بحث',
    celebrities: 'المدربين المتميزين',
    normal_trainer: 'المدربون',
    see_all: 'اظهار الكل',
    trainers: 'المدربون',
    learn: 'تعلم',

    gender: 'جنس',
    women_ins: 'مدرس نساء',
    men_ins: 'مدرس الرجال',
    training_location_pre: 'التدريب موقع التفضيل',

    //Instructor Details
    instructor_detail: 'مدرب التفاصيل',
    member_since: 'عضو منذ',
    experience: 'تجربة',
    total_dri_hour: 'سعر الساعة دينار بحريني',
    ongoing: 'جاري التنفيذ',
    completed: 'منجز',
    about_instructor: 'حول المدرب',
    vehical_lic_detail: 'تفاصيل السيارة ورخصة',
    rating_and_review: 'التقييم والمراجعة',
    view_all: 'عرض الكل',
    book_instructor: 'كتاب المعلم',
    no_of_classes: 'عدد الفصول',
    start_date: 'تاريخ البدء',
    time: 'زمن',
    schedule: 'جدول',
    add_more: 'أضف المزيد',

    //Review
    review: 'مراجعة',
    instructor_fees: 'رسوم المعلم',
    normal_fees: 'الرسوم العادية',
    bhd: 'BHD',
    booking_detail: 'تفاصيل الحجز',
    pay: 'دفع',
    pay_message: 'تم الدفع بنجاح',

    //profile
    profile: 'الملف الشخصي',
    my_payment_history: 'تاريخ الدفع الخاص بي',
    setting: 'ضبط',
    notification: 'إشعارات',
    faq: 'التعليمات',
    terms_use: 'تعليمات الاستخدام',
    logout: 'تسجيل خروج',
    name: 'اسم',
    enter_pass: 'أدخل كلمة مرور جديدة',
    reenter_pass: 'أعد إدخال كلمة المرور الجديدة',
    account_info: 'معلومات الحساب',
    change_pass: 'تغيير كلمة السر',
    save_change: 'حفظ التغييرات',
    current_pass: 'كلمة المرور الحالية',
    save_pass: 'حفظ كلمة المرور',
    payment_history: 'تاريخ الدفع',

    //learn
    learn: 'تعلم',
    training_detail: 'تفاصيل التدريب',
    class_detail: 'الطبقة التفاصيل',
    total_hour_billed: 'مجموع ساعات الفواتير-',
    total_amount: 'المبلغ الإجمالي',
    cancel_training: 'إلغاء التدريب',
    rate_instructor: 'معدل المعلم',
    lanuage_setting: 'إعدادات اللغة',

    classes: 'الطبقات',
    start_date: 'تاريخ البدء',
    time: 'زمن',
    schedule: 'جدول',
    select_date: 'حدد تاريخ',
    trainer_loc_place: 'سوف أذهب إلى موقع المدرب',

    search: 'بحث',
    search_by_location: 'البحث عن طريق الموقع',
    search_by_trainer_name: 'البحث عن طريق اسم المدرب',
    government: 'حكومة',
    gender: 'جنس',
    women_instructor: 'مدرس نساء',
    men_instructor: 'مدرس الرجال',
    learn: 'تعلم',
    personal: 'شخصي',

    // placholder
    email_placholder: 'أدخل البريد الإلكتروني',
    password_placeholder: 'أدخل كلمة المرور',
    conf_pass_placholder: 'إعادة إدخال كلمة المرور',
    first_plac: 'أدخل الاسم الأول',
    last_plac: 'إدخال اسم آخر',
    phone_place: 'أدخل رقم الجوال.',
    cpr_place: 'أدخل CPR',
    lic_place: 'أدخل رقم الترخيص',
    temp_lic: 'أدخل رقم الرخصة المؤقتة',

    lic_date_plac: 'أدخل تاريخ إصدار الترخيص',
    lic_end_date_plac: 'أدخل تاريخ انتهاء الترخيص',
    dob_plac: 'أدخل DOB',

    // network error
    internet_error: 'لا يوجد اتصال إنترنت',
    phone: 'رقم الهاتف المحمول',


    //Notification Screen
    no_notification_found: 'لم يتم العثور على إعلام',
    scroll_down_to_refresh: 'انتقل لأسفل لتحديث',
    email_error: 'ليس بريدًا إلكترونيًا صالحًا',

    fee: 'رسوم',
    amount: 'كمية',
    my_loc_train: 'سوف يأتي المدرب إلى الموقع',
    cont: 'استمر',
    class_detail: 'الطبقة التفاصيل',
    total_hour_train: 'إجمالي ساعات الفوترة -',
    tot_amu: 'المبلغ الإجمالي',
    hello: 'مرحبا',
    inrolv_prog: 'أنت لم تسجل في أي تدريب',
    not_inro: 'أنت مسجل في',
    train_prog: 'برنامج تدريب',
}

export const English = {
    inrolv_prog: 'You did not Enrolled for any Training',
    not_inro: 'You Enrolled in ',
    train_prog: 'Training Program',

    hello: 'Hello',
    class_detail: 'Class Detail',
    total_hour_train: 'Total Hours Billed - ',
    tot_amu: 'Total Amount',
    cont: 'Continue',
    amount: 'Amount',
    fee: 'Fee',
    internet_error: 'No Internet Connection',
    email_error: 'Not a Valid Email',
    // placholder
    email_placholder: 'Enter Email',
    password_placeholder: 'Enter Password',
    conf_pass_placholder: 'Re-Enter Password',
    first_plac: 'Enter First Name',
    last_plac: 'Enter Last Name',
    phone_place: 'Enter Mobile Number.',
    cpr_place: 'Enter CPR',
    lic_place: 'Enter Licence Number',
    temp_lic: 'Enter Temporary Licence Number',
    lic_date_plac: 'Enter Licence Issue Date',
    lic_end_date_plac: 'Enter Licence Expiry Date',
    my_loc_train: 'Trainer Will come to Location',
    dob_plac: 'Enter DOB',
    phone: 'Mobile Number',



    // Home Page
    lanuage_setting: 'Language Setting',

    Hello: 'Hello',
    message: 'Welcome To Driver - Customer App. To Continue using our app Please Login or Signup',
    login: 'Login',
    account: 'Dont Have a Account?',
    signup: 'Signup',
    email: 'Email',

    // Signup
    first_name: 'First Name',
    last_name: 'Last Name',
    mobile: 'Mobile Number',
    cpr: 'CPR Number',
    nationality: 'Nationality',
    dob: 'Date of Birth',
    password: 'Password',
    confirm_password: 'Confirm Password',
    male: 'Male',
    female: 'Female',
    driving_lic_no: 'Driving Licence Number',
    tem_lic_no: 'Temporary Driving Licence Number',
    issue_date: 'Driving Licence Issue Date ',
    expirary_date: 'Driving Licence Expiry Date',
    term: 'By Continuing You are agreeing with our ',
    condition: 'terms and Conditions',
    select_date: 'Select Date',

    //Login screen
    forgot_password: 'Forgot Password',
    continue: 'Continue',
    reset_password: 'Reset Password',
    new_password: 'New Password',
    enter_otp: 'Enter OTP',

    // welcome screen
    welcome_to: 'Welcome to',
    driver_customer_app: 'Driver Customer App',
    trainer_loc_place: 'Will Go to Trainer Location',
    //dashboard 
    explore: 'Explore',
    search: 'Search',
    celebrities: 'Premium Trainers',
    normal_trainer: 'Trainers',
    see_all: 'See All',
    trainers: 'Trainers',
    learn: 'Learn',

    gender: 'Gender',
    women_ins: 'Women Instructor',
    men_ins: 'Men Instructor',
    training_location_pre: 'Training Location Prefrence',

    //Instructor Details
    instructor_detail: 'Instructor Detail',
    member_since: 'Member Since',
    experience: 'Experience',
    total_dri_hour: 'Hourly Rate BHD',
    ongoing: 'Ongoing',
    completed: 'Completed',
    about_instructor: 'About Instructor',
    vehical_lic_detail: 'Vehicle & Licence Details',
    rating_and_review: 'Rating & Review',
    view_all: 'View All',
    book_instructor: 'Book Instructor',
    no_of_classes: 'No of Classses',
    start_date: 'Start Date',
    time: 'Time',
    schedule: 'Schedule',
    add_more: 'Add More',

    //Review
    review: 'Review',
    instructor_fees: 'Instructor Fees',
    normal_fees: 'Normal Fees',
    bhd: 'BHD',
    booking_detail: 'Booking Detail',
    pay: 'Pay',
    pay_message: 'Payment Done Successfully',

    //profile
    profile: 'Profile',
    my_payment_history: 'My Payment History',
    setting: 'Setting',
    notification: 'Notifications',
    faq: 'FAQ',
    terms_use: 'Terms of Use',
    logout: 'Logout',
    name: 'Name',
    enter_pass: 'Enter New Password',
    reenter_pass: 'Re-Enter New Password',

    account_info: 'Account Information',
    change_pass: 'Change Password',
    save_change: 'Save Changes',
    current_pass: 'Current Password',
    save_pass: 'Save Password',
    payment_history: 'Payment History',

    //learn
    learn: 'Learn',
    training_detail: 'Training Detail',
    class_detail: 'Class Detail',
    total_hour_billed: 'Total Hours Billed-',
    total_amount: 'Total Amount',
    cancel_training: 'Cancel Training',
    rate_instructor: 'Rate Instructor',

    classes: 'Classes',
    start_date: 'Start Date',
    time: 'Time',
    schedule: 'Schedule',
    select_date: 'Select Date',


    //Search Screen
    search: 'Search',
    search_by_location: 'Search By Location',
    search_by_trainer_name: 'Search by Trainer Name',
    government: 'Government',
    gender: 'Gender',
    women_instructor: 'Women Instructor',
    men_instructor: 'Men Instructor',
    learn: 'Learn',
    personal: 'Personal',

    //Notification Screen
    no_notification_found: 'No Notification Found',
    scroll_down_to_refresh: 'Scroll Down To refresh'




}