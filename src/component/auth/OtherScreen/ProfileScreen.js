import React, { Component } from 'react';
import { TouchableOpacity, ScrollView, TouchableWithoutFeedback } from 'react-native';
import { View, Text, Thumbnail } from 'native-base';
import { baseStyles, margin, colors, padding, fonts } from '../../../styles/baseStyle';
import ImagePicker from 'react-native-image-picker';
import AppButton from '../../utility/AppButton';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import Activity from '../../utility/activity_indicator';

class ProfileScreen extends Component {

    state = {
        imageuri: null,
        imageresponse: false,
        activity: false,
        name: '',
        email: '',
        token: ''
    }

    options = {
        title: 'Select Avatar',
        storageOptions: {
            skipBackup: true,
            path: 'images',

        },
    };

    componentDidMount = async () => {
        this.setState({ name: await AsyncStorage.getItem('name') });
        this.setState({ imageuri: await AsyncStorage.getItem('profile_uri') });
        this.setState({ email: await AsyncStorage.getItem('email') });
        this.setState({ token: await AsyncStorage.getItem('TOKEN') });
    }

    imageBox = async () => {
        ImagePicker.showImagePicker(this.options, async response => {
            if (response.error) {
            }
            if (!response.didCancel) {
                this.setState({ imageresponse: response });
                this.setState({ imageuri: response.uri });
                this.setState({ activity: true });
                await this.uploadDetail();

            }
        })
    }

    logoutFromApp = async () => {
        await AsyncStorage.clear();
        this.setState({ activity: true });
        this.setState({ activity: false });
        this.props.navigation.navigate('home');
    }

    activity_indicator() {
        if (this.state.activity === true) {
            return <Activity show={this.state.activity} />
        }
        else {
            return null;
        }
    }

    renderImageUser = () => {
        if (!this.state.imageuri) {
            return (
                <Thumbnail
                    large
                    source={{ uri: this.state.imageuri }}
                    style={{ marginVertical: margin.lg }}
                />
            )
        }
        return (
            <Thumbnail
                large
                source={{ uri: this.state.imageuri }}
                style={{ marginVertical: margin.lg }}
            />
        )
    }

    uploadDetail = async () => {
        let data = new FormData();
        data.append('name', this.state.name);
        data.append('email', this.state.email);
        data.append('profile_url', {
            uri: `file://${this.state.imageresponse.path}`,
            type: this.state.imageresponse.type,
            name: this.state.imageresponse.fileName
        });

        let response = await fetch('http://168.235.81.165/cardrive/public/api/user/profile-update', {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                Authorization: `Bearer ${this.state.token}`
            },
            body: data
        });
        let result = await response.json();
        await AsyncStorage.setItem('profile_uri', result.data.user.profile_url);
        this.setState({ imageuri: result.data.user.profile_url });
        this.setState({ activity: false });
    }

    renderInrvolde = () => {
        if (this.props.ongoing.length == 0)
            return (
                <Text style={baseStyles.whiteText}>{this.props.lang.inrolv_prog}</Text>
            )

        else
            return (
                <Text style={baseStyles.whiteText}>{this.props.lang.not_inro} {this.props.ongoing.length} {this.props.lang.train_prog} </Text>
            )

    }



    render() {
        return (
            <View style={[baseStyles.appContainer, Styles.container]}>
                <ScrollView>
                    <View style={Styles.profileContainer}>
                        <Text style={Styles.profileName}>{this.props.lang.hello} {this.state.name} </Text>
                        <TouchableWithoutFeedback onPress={() => this.imageBox()} >
                            <Thumbnail
                                large
                                source={{ uri: this.state.imageuri }}
                                style={{ marginVertical: margin.lg }}
                            />
                        </TouchableWithoutFeedback>
                        <this.renderInrvolde />
                    </View>
                    <View style={{ flex: 1 }}>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('payment')}>
                            <View style={Styles.listItemStyle}>
                                <Text style={Styles.listText}>{this.props.lang.my_payment_history}</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('setting')}>
                            <View style={Styles.listItemStyle}>
                                <Text style={Styles.listText}>{this.props.lang.setting}</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('notification')}>
                            <View style={Styles.listItemStyle}>
                                <Text style={Styles.listText}>{this.props.lang.notification}</Text>
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => this.props.navigation.navigate('faq')}>
                            <View style={Styles.listItemStyle}>
                                <Text style={Styles.listText}>{this.props.lang.faq}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    {this.activity_indicator()}
                    <AppButton
                        title={this.props.lang.logout}
                        titleStyle={{ color: colors.white }}
                        btnStyle={{ marginBottom: 0, backgroundColor: colors.red }}
                        onPress={() => this.logoutFromApp()}
                    />
                </ScrollView>
            </View>
        );
    }
}

const Styles = {
    profileContainer: {
        alignItems: 'center',
        backgroundColor: colors.offBlack,
        marginBottom: margin.lg,
        paddingVertical: padding.lg
    },
    container: {
        paddingHorizontal: 0,
        justifyContent: 'flex-start'
    },
    listItemStyle: {
        backgroundColor: colors.offBlack,
        marginBottom: margin.sm,
        height: 60,
        paddingLeft: padding.lg,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    listText: {
        color: colors.white,
        fontSize: fonts.xl
    },
    profileName: {
        color: colors.white,
        fontSize: fonts.xxl,
        fontWeight: '400'
    }
}

const mapstateToProps = (state) => {
    return {
        lang: state.staticLang.userLanguage,
        ongoing: state.rootReducer.booked_instructre
    }
}
export default connect(mapstateToProps)(ProfileScreen);