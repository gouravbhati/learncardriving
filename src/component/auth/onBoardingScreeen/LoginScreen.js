import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, Platform } from 'react-native';
import { connect } from 'react-redux';
import { baseStyles, radius, padding } from '../../../styles/baseStyle';
import AppInput from '../../utility/AppInput';
import AppButton from '../../utility/AppButton';
import AsyncStorage from '@react-native-community/async-storage';
import Snackbar from 'react-native-snackbar';
import { setNormalInstructure, setPrimeInstructure, setSlider, setBookedInstructure } from '../../../store/reducer/actions/rootReducerAction';
import { dashboard, booked_instructur } from '../../utility/api utility/apis';
import fetchUserData from '../../utility/api utility/fetchuserdatabyapi';
import Activity from '../../utility/activity_indicator';

class LoginScreen extends Component {
    state = {
        email: '',
        email_er: '',
        activity: false,
        cpr: '',
        cpr_er: '',
        password: '',
        password_er: '',
        device_type: '',
        device_token: '',
        apierror: '',
        email_placeholder: this.props.lang.email_placholder,
        pass_placeholder: this.props.lang.password_placeholder,
        cpr_placeholder: this.props.lang.cpr_place,
    }
    async componentDidMount() {
        let token = await AsyncStorage.getItem('device_token');
        this.setState({ device_token: token });
        Platform.OS === 'ios' ? this.setState({ device_type: 'ios' }) : this.setState({ device_type: 'android' })
    }
    validate() {
        if (this.state.email === '') {
            Snackbar.show({
                text: 'Email is Required',
                duration: 2000
            });
            return 0
        }
        if (this.state.password === '') {
            Snackbar.show({
                text: 'Password is Required',
                duration: 2000
            });
            return 0
        }
        if (this.state.cpr === '') {
            Snackbar.show({
                text: 'CPR Number is Required',
                duration: 2000
            });
            return 0
        }
        else {
            this.setState({ activity: true });
            this.loginApi();
        }
    }

    loginApi = async () => {
        try {
            let res = await fetch("http://168.235.81.165/cardrive/public/api/user/login", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email: this.state.email,
                    password: this.state.password,
                    device_type: this.state.device_type,
                    device_token: this.state.token,
                    cpr_number: this.state.cpr
                })
            });
            let result = await res.json();
            let token = result.data.token;
            if (result.success === true) {
                this.setState({ activity: true });
                await AsyncStorage.setItem('TOKEN', token);
                await AsyncStorage.setItem('STATUS', 'LOGEDIN');
                await AsyncStorage.setItem('name', result.data.user.name);
                await AsyncStorage.setItem('email', result.data.user.email);
                await AsyncStorage.setItem('profile_uri', result.data.user.profile_url);
                await AsyncStorage.setItem('password', this.state.password);
                let response = await fetchUserData(dashboard, token, 'en');
                let booked_instructure = await fetchUserData(booked_instructur, token, 'en');
                if (response.success) {
                    this.props.setSlider(response.data.sliders);
                    this.props.setNormalInstructure(response.data.normal_instructor);
                    this.props.setBookedInstructure(booked_instructure.data);
                    this.props.setPrimeInstructure(response.data.premium_instructor);
                    this.setState({ activity: false });
                    this.props.navigation.navigate('appWelcome');
                }
                else {
                    this.setState({ activity: false });
                    this.setState({ apierror: response.message })
                }
            }
            else {
                this.setState({ activity: false });
                this.setState({ apierror: result.message });
            }
        }
        catch (error) {
            this.setState({ activity: false });
            if (error.message == 'Network request failed')
                Snackbar.show({
                    text: this.props.lang.internet_error,
                    duration: 4000
                })
        }
    }

    render() {
        return (
            <View style={baseStyles.appContainer}>
                <ScrollView>
                    <Activity show={this.state.activity} />
                    <Text style={baseStyles.heading}>{this.props.lang.login}</Text>
                    <AppInput label={this.props.lang.email}
                        onEndEditing={() => this.setState({ email_placeholder: this.props.lang.email_placholder })}
                        value={this.state.email}
                        onChangeText={(text) => { this.setState({ email: text }) }}
                        keyboardType='email-address'
                        placeholder={this.state.email_placeholder}
                        onFocus={() => this.setState({ email_placeholder: '' })}
                    />

                    <AppInput label={this.props.lang.password}
                        value={this.state.password}
                        onChangeText={(text) => { this.setState({ password: text }) }}
                        secureTextEntry={true}
                        placeholder={this.state.pass_placeholder}
                        onFocus={() => this.setState({ pass_placeholder: '' })}
                        onEndEditing={() => this.setState({ pass_placeholder: this.props.lang.password_placeholder })}
                    />

                    <AppInput label={this.props.lang.cpr}
                        value={this.state.cpr}
                        onChangeText={(text) => { this.setState({ cpr: text }) }}
                        keyboardType='numeric'
                        placeholder={this.state.cpr_placeholder}
                        onFocus={() => this.setState({ cpr_placeholder: '' })}
                        onEndEditing={() => this.setState({ cpr_placeholder: this.props.lang.cpr_place })}
                    />

                    <TouchableOpacity onPress={() => this.props.navigation.navigate('forgotPass', { id: 55 })}>
                        <Text style={[baseStyles.whiteText, baseStyles.textAlignRight]}>{this.props.lang.forgot_password} ?</Text>
                    </TouchableOpacity>
                    
                    <Text style={{ color: 'red' }} >{this.state.apierror}</Text>

                    <View style={{ alignItems: 'flex-end' }}>
                        <AppButton
                            title={this.props.lang.login}
                            btnStyle={{
                                borderRadius: radius.xxl,
                                borderTopLeftRadius: 0,
                                paddingHorizontal: padding.xxl
                            }}
                            onPress={() => {
                                this.validate();
                            }}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProp = state => {
    return {
        lang: state.staticLang.userLanguage
    }
}

export default connect(mapStateToProp, { setNormalInstructure, setBookedInstructure, setPrimeInstructure, setSlider })(LoginScreen);