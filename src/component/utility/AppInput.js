import React from 'react';
import { Item, Input, View, Label } from 'native-base'
import { radius, margin, colors, padding, fonts } from '../../styles/baseStyle';
import { TextInput } from 'react-native';

const AppInput = ({
    placeholder,
    keyboardType,
    onKeyPress,
    onChangeText,
    errorMsg,
    value,
    label,
    inputStyle,
    secureTextEntry,
    editable,
    onEndEditing,
    onFocus,
    render
}) => {
    return (
        <View>
<Label style={Styles.floatingStyle}>{label}</Label>
            <Item regular style={[Styles.inputStyle,inputStyle]}>
                <Input
                    placeholder={placeholder}
                    placeholderTextColor='rgba(255,255,255,0.5)'
                    value={value}
                    error={errorMsg}
                    keyboardType={keyboardType}
                    onKeyPress={onKeyPress}
                    onChangeText={onChangeText}
                    style={Styles.textStyle}
                    secureTextEntry={secureTextEntry}
                    editable={editable}
                    render={render}
                    onFocus={onFocus}
                    onEndEditing={onEndEditing}

                />
            </Item>

        </View>
    );
}
const Styles = {
    inputStyle: {
        borderRadius: radius.xl,
        marginVertical: margin.md,
        height:50,
        alignSelf:'center',
    },
    textStyle: {
    
      fontWeight:'300',
        color: 'white',
        
    },
    floatingStyle: {
     color: 'white'

    }
}

export default AppInput;