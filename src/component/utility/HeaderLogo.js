import React from 'react';
import { TouchableOpacity,Image } from 'react-native';

export default HeaderLogo = () => {
    return (
        <TouchableOpacity>
            <Image 
            source={require('../../assets/header_logo.png')}
            style={{height:100, width:200,resizeMode:'contain',alignSelf:'center'}}
            />
        </TouchableOpacity>
    );
}