import React, { Component } from 'react';
import { View, Text, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { baseStyles, radius, padding } from '../../../styles/baseStyle';
import AppInput from '../../utility/AppInput';
import AppButton from '../../utility/AppButton';
import Snackbar from 'react-native-snackbar';
import Activity from '../../utility/activity_indicator';
class ForgotPasswordScreen extends Component {
    state = {
        new_pass: '',
        new_pass_er: '',

        conf_pass: '',
        conf_pass_er: '',

        error: '',
        error_data: '',
        activity: false,
        pass_placeholder: this.props.lang.password_placeholder,
        conf_pass_placeholder: this.props.lang.conf_pass_placholder,
    }
    validate() {
        if (this.state.new_pass === '') {
            this.showSnackBar('New Password is Required');
            return 0
        }
        if (this.state.conf_pass === '') {
            this.showSnackBar('New Password is Required');
            return 0
        }
        if (this.state.new_pass !== this.state.conf_pass) {
            this.showSnackBar('New and Confirm Password should be same');
            return 0
        }
        else {
            this.setState({ error: false });
            this.setState({ activity: true });
            this.reset_password();
        }
    }
    showSnackBar = (message) => {
        Snackbar.show({
            text: message,
            duration: 2000
        })
    }
    reset_password = async () => {
        let user_email = this.props.navigation.getParam('email');
        let res = await fetch('http://168.235.81.165/cardrive/public/api/user/reset-password', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: user_email,
                password: this.state.new_pass,
            })
        });
        let result = await res.json();
        if (result.success === true) {
            this.setState({ activity: false })
            this.showSnackBar('Successfully Changed Password');
            this.props.navigation.navigate('login');
        }
        else {
            this.setState({ activity: false })
            this.showSnackBar('Request Failed');
        }
    }
    activity_indicator() {
        if (this.state.activity === true) {
            return <ActivityIndicator size="large" color="#ffe200" />
        }
        else {
            return null;
        }
    }
    render() {
        return (
            <View style={baseStyles.appContainer}>
                <Text style={baseStyles.heading}>{this.props.lang.forgot_password}</Text>
                <AppInput
                    label={this.props.lang.new_password}
                    value={this.state.new_pass}
                    onChangeText={(text) => { this.setState({ new_pass: text }) }}
                    secureTextEntry={true}
                    placeholder={this.state.pass_placeholder}
                    onFocus={() => this.setState({ pass_placeholder: '' })}
                    onEndEditing={() => this.setState({ pass_placeholder: this.props.lang.password_placeholder })}
                />
                <AppInput
                    label={this.props.lang.confirm_password}
                    value={this.state.conf_pass}
                    placeholder={this.state.conf_pass_placeholder}
                    onChangeText={(text) => { this.setState({ conf_pass: text }) }}
                    secureTextEntry={true}
                    onFocus={() => this.setState({ conf_pass_placeholder: '' })}
                    onEndEditing={() => this.setState({ conf_pass_placeholder: this.props.lang.conf_pass_placholder })}
                />
                <Activity show={this.state.activity} />
                <View style={{ alignItems: 'flex-end' }}>
                    <AppButton
                        title={this.props.lang.continue}
                        btnStyle={{
                            borderRadius: radius.xxl,
                            borderTopLeftRadius: 0,
                            paddingHorizontal: padding.xxl
                        }}
                        onPress={() => {
                            this.validate();
                        }}
                    />
                </View>
            </View>
        );
    }
}

const mapStateToProp = state => {
    return {
        lang: state.staticLang.userLanguage
    }
}

export default connect(mapStateToProp)(ForgotPasswordScreen);

