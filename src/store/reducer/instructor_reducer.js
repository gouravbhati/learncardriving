const istate={
    instructor_detail: ''
}

export default (state=istate,action)=>{
    switch(action.type)
    {
        case "Instructor_detail":
            return{
                ...state,
                instructor_detail: action.payload
            }
        default:{
            return state;
        }
    }
    return state;
}