import React, { Component } from 'react';
import { View, Image } from 'react-native';
import { baseStyles, radius, padding } from '../../../styles/baseStyle';
import AppButton from '../../utility/AppButton';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';

class AppWelcomeScreen extends Component {

    state = {
        english: true
    }

    componentDidMount = async () => {
        let lang = await AsyncStorage.getItem('LANG');
        if (lang == 'ar') {
            this.setState({ english: false });
        }
    }

    render() {
        return (
            <View style={[baseStyles.appContainer]}>
                <Image
                    source={this.state.english ? require('../../../assets/welcomeApp.png') : require('../../../assets/welcome-ar.png')}
                    style={{ width: 300, height: null, flex: 1, resizeMode: 'contain', alignSelf: 'center' }}
                />

                <AppButton
                    title={this.props.lang.continue}
                    btnStyle={{
                        borderRadius: radius.xxl,
                        borderTopLeftRadius: 0,
                        paddingHorizontal: padding.xxl
                    }}
                    onPress={() => this.props.navigation.navigate('dashboard')} />
            </View>
        );
    }
}



const mapStateToProp = state => {
    return {
        lang: state.staticLang.userLanguage
    }
}

export default connect(mapStateToProp)(AppWelcomeScreen);