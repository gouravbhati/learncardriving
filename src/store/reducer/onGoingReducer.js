
initState = {
    onGoingDetail: []
}

export default (state = 0, action) => {

    switch (action.type) {
        case 'SET_ONGOING_DETAIL':
            return {
                ...state,
                onGoingDetail: action.payload,
            }
        default: {
            return state;
        }
    }
}