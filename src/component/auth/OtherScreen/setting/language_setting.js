import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ActivityIndicator, Modal } from 'react-native';
import { connect } from 'react-redux';
import { setStaticLangArabic, setStaticLangEnglish, Refresh_Lang } from '../../../../store/reducer/actions/staticLanguageReduceraction';
import { colors, margin, fonts, baseStyles, padding } from '../../../../styles/baseStyle';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';

class LanguageSetting extends React.Component {

    state = {
        activity: false,
    }

    changeArabic() {
        this.setState({ activity: true })
        setTimeout(() => {
            this.setState({ activity: false })
            AsyncStorage.setItem('LANG', 'ar')
            this.props.setStaticLangArabic();
            this.showSnackBar('Language Changed Successfully');
        }, 2000);

    }
    changeEnglish() {
        this.setState({ activity: true })
        setTimeout(() => {
            this.setState({ activity: false })
            this.props.setStaticLangEnglish();
            AsyncStorage.setItem('LANG', 'en')
            this.showSnackBar('Language Changed Successfully');

        }, 2000);
    }
    showSnackBar = (message) => {
        Snackbar.show({
            text: message,
            duration: 2000
        })
    }

    activity_indicator() {
        if (this.state.activity === true) {
            return <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                <ActivityIndicator size='large' color='yellow' />
            </View>
        }
        else {
            return null;
        }
    }
    render() {
        return (

            <View style={[baseStyles.appContainer, Styles.container]}>
                <TouchableOpacity onPress={() => this.changeEnglish()}>
                    <View style={Styles.listItemStyle}>
                        <Text style={Styles.listText}>English</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.changeArabic()}>
                    <View style={Styles.listItemStyle}>
                        <Text style={Styles.listText}>Arabic</Text>
                    </View>
                </TouchableOpacity>

                {this.activity_indicator()}
            </View>
        )
    }
}


const Styles = {

    container: {
        paddingHorizontal: 0,
        justifyContent: 'flex-start'
    },
    listItemStyle: {
        backgroundColor: colors.offBlack,
        marginBottom: margin.sm,
        height: 60,
        paddingLeft: padding.lg,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    listText: {
        color: colors.white,
        fontSize: fonts.xl
    },
}

const mapStateToProp = state => {
    return {
        lang: state.staticLang.userLanguage
    }
}

export default connect(mapStateToProp, { setStaticLangArabic, setStaticLangEnglish })(LanguageSetting);