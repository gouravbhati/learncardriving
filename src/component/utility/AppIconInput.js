import React from 'react';
import { Item, Icon, Input } from 'native-base';
import { baseStyles, colors, padding, radius, fonts, margin } from '../../styles/baseStyle';

export default AppIconInput = ({ iconName, placeholder, inputGroup, editable }) => {
    return (
        <Item regular style={[Styles.inputGroup, inputGroup]}>
            <Input editable={editable} placeholder={placeholder} placeholderTextColor="#fff" style={Styles.inputStyle} />
            <Icon active name={iconName} style={{ color: colors.white }} />
        </Item>
    );
}

const Styles = {
    inputStyle: {
        borderRightWidth: 1,
        borderRightColor: '#f2f2f2',
        marginVertical: 8,
        height: 40,
        alignSelf: 'center',
        fontSize:13,
    },
    inputGroup: {
        width: '43%',
        borderRadius: radius.lg,
    }
}