import React, { Component } from 'react';
import { View, TouchableOpacity, Text } from 'react-native'
import { colors, margin, fonts, baseStyles, padding } from '../../../../styles/baseStyle';
import { connect } from 'react-redux';
class SettingScreen extends Component {
    render() {
        return (
            <View style={[baseStyles.appContainer, Styles.container]}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('account')}>
                    <View style={Styles.listItemStyle}>
                        <Text style={Styles.listText}>{this.props.lang.account_info}</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.props.navigation.navigate('changePass')}>
                    <View style={Styles.listItemStyle}>
                        <Text style={Styles.listText}>{this.props.lang.change_pass}</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('language_setting')}>
                    <View style={Styles.listItemStyle}>
        <Text style={Styles.listText}>{this.props.lang.lanuage_setting}</Text>
                    </View>
                </TouchableOpacity>

            </View>
        );
    }
}

const Styles = {

    container: {
        paddingHorizontal: 0,
        justifyContent: 'flex-start'
    },
    listItemStyle: {
        backgroundColor: colors.offBlack,
        marginBottom: margin.sm,
        height: 60,
        paddingLeft: padding.lg,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    listText: {
        color: colors.white,
        fontSize: fonts.xl
    },
}
const mapstateToProps = (state) => {
    console.log(state);
    return {
        lang: state.staticLang.userLanguage
    }
}
export default connect(mapstateToProps)(SettingScreen);