import React, { Component } from 'react';
import { Item, Input, View, Label } from 'native-base'
import { Text, ScrollView, TouchableOpacity, Image, Platform, Picker } from 'react-native';
import { baseStyles, colors, radius, padding, margin, fonts } from '../../../styles/baseStyle';
import AppInput from '../../utility/AppInput';
import AppButton from '../../utility/AppButton';
import { connect } from 'react-redux';
import Snackbar from 'react-native-snackbar';
import Activity from '../../utility/activity_indicator';
import DatePick from '../../utility/datapickle';
class SignUpScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            radioBtnsData: ['Male', 'Female'],
            checked: 0,
            activity: false,
            value: '',
            first_name: '',
            first_name_er: '',
            last_name: '',
            last_name_er: '',
            email: '',
            email_er: '',
            mobile: '',
            mobile_er: '',
            cpr: '',
            cpr_er: '',
            Nationality: '',
            Nationality_er: '',
            dob: '',
            dob_er: '',
            password: '',
            password_er: '',
            confirm_password: '',
            confirm_password_er: '',
            driving_licence_no: '',
            driving_licence_no_er: '',
            temporary_driving_lic_no: '',
            temporary_driving_lic_no_er: '',
            issus_date: '',
            issus_date_er: '',
            Expiry_date: '',
            Expiry_date_er: '',
            gender: 'male',

            error: '',
            error_data: '',

            device_token: '',
            device_type: '',
            apierror: '',

            first_placeholder: this.props.lang.first_plac,
            last_placeholder: this.props.lang.last_plac,
            email_placeholder: this.props.lang.email_placholder,
            mobile_placeholder: this.props.lang.phone_place,
            cpr_placeholder: this.props.lang.cpr_place,
            pass_placeholder: this.props.lang.password_placeholder,
            conf_pass_placeholder: this.props.lang.conf_pass_placholder,
            lic_placeholder: this.props.lang.lic_place,
            temp_lic_placeholder: this.props.lang.temp_lic,
            dob_plac: this.props.lang.dob_plac,
            issue_place: this.props.lang.lic_date_plac,
            expiry_plac: this.props.lang.lic_end_date_plac


        }
    }
    componentDidMount() {
        Platform.OS === 'ios' ? this.setState({ device_type: 'ios' }) : this.setState({ device_type: 'android' });
    }

    showSnackBar = (message) => {
        Snackbar.show({
            text: message,
            duration: 2000
        })
    }

    validate() {


        if (this.state.first_name === '') {
            this.showSnackBar('First Name is Required');
            return 0
        }
        if (this.state.last_name === '') {
            this.showSnackBar('last Name is Required');
            return 0
        }
        if (this.state.email === '') {
            this.showSnackBar('Email is Required');
            return 0
        }
        if (this.state.mobile === '') {
            this.showSnackBar('Mobile Number is Required');
            return 0
        }
        if (this.state.cpr === '') {
            this.showSnackBar('Cpr Number is Required');
            return 0
        }
        if (this.state.dob === '') {
            this.showSnackBar('Date of Birth is Required');
            return 0
        }
        if (this.state.password === '') {
            this.showSnackBar('Password is Required');
            return 0
        }
        if (this.state.confirm_password === '') {
            this.showSnackBar('Confirm Password is Required');
            return 0
        }
        if (this.state.driving_licence_no === '') {
            this.showSnackBar('Driving Licence Number is Required');
            return 0
        }
        if (this.state.temporary_driving_lic_no === '') {
            this.showSnackBar('Temporary Driving Licence Number is Required');
            return 0
        }
        if (this.state.issus_date === '') {
            this.showSnackBar('Issue Date is Required');
            return 0
        }
        if (this.state.Expiry_date === '') {
            this.showSnackBar('Expiry Date is Required');
            return 0
        }
        else {
            this.setState({ activity: true });
            this.signup();
        }
    }
    select_value = async (value_, type) => {
        if (type === 'code') {
            await this.setState({ value: value_ })
        }
        else {
            await this.setState({ nationality: value_ })
        }

    }
    signup = async () => {
        let code = this.state.value;
        let m = this.state.mobile;
        let mobile = code.concat(m);


        try {
            let res = await fetch("http://168.235.81.165/cardrive/public/api/user/register", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    name: this.state.first_name,
                    email: this.state.email,
                    password: this.state.password,
                    client_id: '1',
                    client_secret: 'Ra3pmo7LjDbYZ7fcyJTo3e7oFx2BSnHOIyLzzsvU',
                    phone: mobile,
                    cpr_number: this.state.cpr,
                    gender: this.state.gender,
                    nationality: this.state.Nationality,
                    dob: this.state.dob,
                    driving_licence_number: this.state.driving_licence_no,
                    temporary_driving_licence_number: this.state.temporary_driving_lic_no,
                    driving_license_issue_date: this.state.issus_date,
                    driving_license_expiry_date: this.state.Expiry_date
                }),
            });
            let result = await res.json();
            this.setState({ activity: false });
            if (result.success === true) {
                alert('otp :' + result.data.otp);
                this.props.navigation.navigate('otp', { name: 'signup' });

            }
            else {
                this.setState({ activity: false });
                console.log(result.message);
                if (result.message == 'Validation Error.')
                    this.setState({ apierror: 'Email, Phone, CPR must not unique ' });
                else
                    this.setState({ apierror: result.message });
            }
        }
        catch (erro) {
            this.setState({ activity: false });
            if (error.message == 'Network request failed')
                Snackbar.show({
                    text: this.props.lang.internet_error,
                    duration: 4000
                })
            else
                this.showSnackBar('CPR and Mobiel number already taken')
        }
    }



    updateDate = (rawdate) => {
        let response = rawdate.toString();
        let date = response.split(" ");
        let day = date[2];
        let year = date[3];
        let mont = this.getMonth(date[1]);
        return year + '-' + mont + '-' + day
    }

    getMonth = (mon) => {
        switch (mon) {
            case 'Jan':
                return '01'
            case 'Feb':
                return '02'
            case 'Mar':
                return '03'
            case 'Apr':
                return '04'
            case 'May':
                return '05'
            case 'Jun':
                return '06'
            case 'Jul':
                return '07'
            case 'Aug':
                return '08'
            case 'Sep':
                return '09'
            case 'Oct':
                return '10'
            case 'Nov':
                return '11'
            case 'Dec':
                return '12'
        }
    }

    render() {
        return (

            <View style={baseStyles.appContainer}>
                <ScrollView>
                    <Text style={baseStyles.heading}>{this.props.lang.signup}</Text>

                    <AppInput label={this.props.lang.first_name}
                        value={this.state.first_name}
                        onChangeText={(text) => { this.setState({ first_name: text }) }}
                        placeholder={this.state.first_placeholder}
                        onFocus={() => this.setState({ first_placeholder: '' })}
                        onEndEditing={() => this.setState({ first_placeholder: this.props.lang.first_plac })}
                    />

                    <AppInput label={this.props.lang.last_name}
                        onChangeText={(text) => { this.setState({ last_name: text }) }}
                        value={this.state.last_name}
                        placeholder={this.state.last_placeholder}
                        onFocus={() => this.setState({ last_placeholder: '' })}
                        onEndEditing={() => this.setState({ last_placeholder: this.props.lang.last_plac })}
                    />

                    <AppInput label={this.props.lang.email}
                        onChangeText={(text) => { this.setState({ email: text }) }}
                        value={this.state.email}
                        keyboardType='email-address'
                        placeholder={this.state.email_placeholder}
                        onFocus={() => this.setState({ email_placeholder: '' })}
                        onEndEditing={() => this.setState({ email_placeholder: this.props.lang.email_placholder })}
                    />

                    <Text style={{ color: 'white', fontSize: 16 }} >{this.props.lang.phone}</Text>

                    <View style={[Styles.inputStyle], { flexDirection: 'row', borderColor: 'white', borderWidth: 1, borderRadius: radius.xl }}>
                        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                            <Picker
                                selectedValue={this.state.value}
                                style={{ width: 100, color: 'white' }}
                                mode='dropdown'
                                onValueChange={(itemValue) => {
                                    this.select_value(itemValue, 'code');
                                }}>
                                <Picker.Item label="+966" value="+966 " />
                                <Picker.Item label="+91" value="+91 " />
                            </Picker>
                        </View>
                        <View>

                            <Input
                                style={{ color: 'white' }}
                                placeholder={this.props.lang.mobile}
                                onChangeText={(text) => { this.setState({ mobile: text }) }}
                                keyboardType='numeric'
                                maxLength={13}
                            />
                        </View>
                    </View>
                    <AppInput
                        label={this.props.lang.cpr}
                        placeholder={this.state.cpr_placeholder}
                        value={this.state.cpr}
                        keyboardType='numeric'
                        maxLength={8}
                        onFocus={() => this.setState({ cpr_placeholder: '' })}
                        onEndEditing={() => this.setState({ cpr_placeholder: this.props.lang.cpr_place })}
                        onChangeText={(text) => { this.setState({ cpr: text }) }}
                    />
                    <Label style={{ color: 'white' }}>{this.props.lang.nationality}</Label>
                    <View style={[Styles.inputStyle], {
                        flexDirection: 'row', borderColor: 'white',
                        borderWidth: 1, borderRadius: radius.xl, alignItems: 'center', justifyContent: 'center'
                    }}>
                        <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                            <Picker
                                selectedValue={this.state.nationality}
                                style={{ width: 300, color: 'white' }}
                                mode='dropdown'
                                onValueChange={(itemValue) => {
                                    this.select_value(itemValue, "nationality");
                                }}>
                                <Picker.Item label="Soudi Arabia" value="Soudi Arabia" />
                                <Picker.Item label="India" value="India" />
                            </Picker>
                        </View>
                    </View>
                    <DatePick
                        label={this.props.lang.dob}
                        minimumDate={new Date('01/01/1950')}
                        maximumDate={new Date('12/31/2003')}
                        placeHolderText={this.state.dob_plac}
                        onChangeText={(text) => { this.setState({ dob: this.updateDate(text) }) }}
                    />

                    <AppInput
                        label={this.props.lang.password}
                        value={this.state.password}
                        placeholder={this.state.pass_placeholder}
                        onChangeText={(text) => { this.setState({ password: text }) }}
                        secureTextEntry={true}
                        onFocus={() => this.setState({ pass_placeholder: '' })}
                        onEndEditing={() => this.setState({ pass_placeholder: this.props.lang.password_placeholder })}
                    />
                    <AppInput
                        label={this.props.lang.confirm_password}
                        value={this.state.confirm_password}
                        placeholder={this.state.conf_pass_placeholder}
                        onChangeText={(text) => { this.setState({ confirm_password: text }) }}
                        secureTextEntry={true}
                        onFocus={() => this.setState({ conf_pass_placeholder: '' })}
                        onEndEditing={() => this.setState({ conf_pass_placeholder: this.props.lang.conf_pass_placholder })}
                    />

                    <View style={[baseStyles.ItemRowStyle, baseStyles.marginVerticalLg]}>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', flex: 1 }} >
                            <TouchableOpacity style={Styles.btn} onPress={() => this.setState({ gender: 'male' })}>
                                <Image style={Styles.img} source={this.state.gender == 'male' ? require("../../../assets/radio.png") : require("../../../assets/radio_circle.png")} />
                                <Text style={Styles.text}>{this.props.lang.male}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={() => { this.setState({ gender: 'female' }) }} style={Styles.btn}>
                                <Image style={Styles.img} source={this.state.gender == 'female' ? require("../../../assets/radio.png") : require("../../../assets/radio_circle.png")} />
                                <Text style={Styles.text}>{this.props.lang.female}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <AppInput
                        label={this.props.lang.driving_lic_no}
                        value={this.state.driving_licence_no}
                        keyboardType='numeric'
                        placeholder={this.state.lic_placeholder}
                        onFocus={() => this.setState({ lic_placeholder: '' })}
                        onEndEditing={() => this.setState({ lic_placeholder: this.props.lang.lic_place })}
                        onChangeText={(text) => { this.setState({ driving_licence_no: text }) }}
                    />

                    <AppInput label={this.props.lang.tem_lic_no}
                        placeholder={this.state.temp_lic_placeholder}
                        value={this.state.temporary_driving_lic_no}
                        keyboardType='numeric'
                        value={this.state.temporary_driving_lic_no}
                        onFocus={() => this.setState({ temp_lic_placeholder: '' })}
                        onEndEditing={() => this.setState({ temp_lic_placeholder: this.props.lang.temp_lic })}
                        onChangeText={(text) => { this.setState({ temporary_driving_lic_no: text }) }}
                    />

                    <DatePick
                        label={this.props.lang.issue_date}
                        placeHolderText={this.state.issue_place}
                        maximumDate={new Date()}
                        onChangeText={(text) => { this.setState({ issus_date: this.updateDate(text) }) }}
                    />

                    <DatePick
                        label={this.props.lang.expirary_date}
                        minimumDate={this.state.issus_date ? new Date(this.state.issus_date.toString()) : new Date()}
                        placeHolderText={this.state.expiry_plac}
                        onChangeText={(text) => { this.setState({ Expiry_date: this.updateDate(text) }) }}
                    />

                    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                        <Text style={baseStyles.whiteText}>{this.props.lang.term}</Text>
                        <Text style={{ color: colors.red }}>{this.props.lang.condition}.</Text>
                    </View>
                    <Text style={{ color: 'red' }} >{this.state.apierror}</Text>
                    <Activity show={this.state.activity} />
                    <View style={{ alignItems: 'flex-end' }}>
                        <AppButton
                            title={this.props.lang.signup}
                            btnStyle={{
                                borderRadius: radius.xxl,
                                borderTopLeftRadius: 0,
                                paddingHorizontal: padding.xxl
                            }}
                            onPress={() => {
                                this.validate();
                            }} />
                    </View>
                </ScrollView>
            </View>

        );
    }
}

const Styles = {
    img: {
        height: 25,
        width: 25,
        resizeMode: 'contain',
        marginRight: margin.md
    },
    text: {
        fontSize: fonts.xl,
        color: colors.white,
    },
    btn: {
        flexDirection: 'row',
        alignItems: 'center'

    },
    inputStyle: {
        borderRadius: radius.xl,
        marginVertical: margin.md,
        height: 50,
        alignSelf: 'center',
    },
    textStyle: {
        textAlign: 'center',
        textTransform: 'capitalize',
        color: colors.white,
        fontWeight: 'bold'
    },
    floatingStyle: {
        color: colors.yellow,
        //textTransform: 'capitalize',
        textAlign: 'center',
        position: 'absolute',
        top: -5,
        paddingLeft: margin.lg,
        fontWeight: 'bold',
        paddingRight: margin.lg,
        alignSelf: 'center',
        backgroundColor: colors.black,

    }
}


const mapStateToProp = state => {
    return {
        lang: state.staticLang.userLanguage
    }
}

export default connect(mapStateToProp)(SignUpScreen);

