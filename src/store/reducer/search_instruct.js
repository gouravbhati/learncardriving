const istate={
    data: ''
}

export default(state=istate, action)=>{
    switch(action.type)
    {
        case 'Add':
            return{
                ...state,
                data: action.payload
            }
        default:{
            return state;
        }
    }
    return state;
}

