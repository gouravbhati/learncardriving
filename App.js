
import React, { Component } from 'react';

import AppRootNavigation from './src/router/Router';
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import {Add_Notification} from './src/store/reducer/actions/notification';

class App extends Component {

  state={
    t: '',
    b: ''
  }
  componentDidMount() {
    this.permission();
    this.receive_message();
    this.create_channel();
    //this.receive_notification();
  }
  receive_message() {
    this.messageListener = firebase.messaging().
      onMessage((message) => {
        // Process your message as required
        this.create_channel();
      });
  }
  create_channel() {
    const channel = new firebase.notifications.Android.Channel(
      "fcm_FirebaseNotifiction_default_channel", // channelId
      "default_notification_channel_id", // channel name
      firebase.notifications.Android.Importance.High // channel importance
    ).setDescription("Used for getting reminder notification")
      //.setSound('me_too.wav');
    firebase.notifications().android.createChannel(channel);
    //console.log("channel..");
    this.receive_notification(channel);
  }
  receive_notification(channel) {
    if (Platform.OS === 'ios') {
      this.removeNotificationDisplayedListener = firebase.notifications().onNotificationDisplayed((notification) => {
        const { title, body } = notification;
        //console.log("Notification..")
        this.display_notification(title, body, channel, notification);
      });
    }
    else {
      this.removeNotificationListener = firebase.notifications().
        onNotification((notification) => {
          // Process your notification as required
          const { title, body } = notification;
          
          //console.log("Notification..")
          this.display_notification(title, body, channel, notification);
        });
    }

  }

  display_notification(title, body, channel, n) {
    const notification = new firebase.notifications.Notification({
      //sound: 'me_too.wav',
      show_in_foreground: true,
    })
      .setNotificationId('1')
      //.setSound('me_too.wav')
      .setTitle(title)
      .setBody(body)
      //.setSound(sound)
      .setData({
        key1: 'value1',
        key2: 'value2',
      })
      .android.setPriority(firebase.notifications.Android.Priority.High) // set priority in Android
      .android.setChannelId("fcm_FirebaseNotifiction_default_channel") // should be the same when creating channel for Android
      .android.setAutoCancel(true); // To remove notification when tapped on i
    //.android.setSmallIcon('ic_launcher');

    firebase.notifications().displayNotification(notification);
    let d = new Date();
    let da = d.toLocaleDateString();
    let data={
      title1: title,
      body1:  body,
      date: da,
    }
    let a =this.props.notification_data;
    a.push(data);
    this.props.Add_Notification(a);
    
    //console.log("displayed..");


    this.notificationOpenedListener = firebase.notifications().
      onNotificationOpened((notificationOpen) => {
        const { title, body } = notificationOpen.notification;
        //console.log('onNotificationOpened:');
        //alert(body);
      
      });

     const notificationOpen = firebase.notifications().getInitialNotification();
     if (notificationOpen) {
       const {title, body} = notificationOpen;
      
     }
  }

  permission = async () => {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      // user has permissions
      this.token();
    } else {
      // user doesn't have permission
      try {
        await firebase.messaging().requestPermission();
        // User has authorised
      } catch (error) {
        // User has rejected permissions
      }
    }

    
  }

  token = async () => {
    const fcmToken = await firebase.messaging().getToken();
    if (fcmToken) {
      // user has a device token
      AsyncStorage.setItem('device_token',fcmToken);
      console.log(fcmToken);
    } else {
      // user doesn't have a device token yet
    }
  }
  render() {
    return (
      <AppRootNavigation/>
    );
  }
}
const mapStateToProps=(state)=>{
  console.log(state);
  return {
    notification_data: state.notification.notification,
  }
}

export default connect(mapStateToProps, {Add_Notification})(App);