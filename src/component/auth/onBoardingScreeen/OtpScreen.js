import React, { Component } from 'react';
import { View, Text, TextInput } from 'react-native';
import { Item } from 'native-base';
import { baseStyles, radius, padding } from '../../../styles/baseStyle';
import AppInput from '../../utility/AppInput';
import AppButton from '../../utility/AppButton';
import { connect } from 'react-redux';
import Snackbar from 'react-native-snackbar';

class OtpScreen extends Component {

    state = {
        otp: [],
        otperror: '',
        isLoading: false,
        curTime: 60,
    }
    otpTextInput = [];

    componentDidMount() {
        this.otpTextInput[0].focus();
        setInterval(() => {
            if (this.state.curTime > 0) {
                this.setState({
                    curTime: this.state.curTime - 1
                })

            }
        }, 1000)

    }

    focusPrevious(key, index) {
        if (key === 'Backspace' && index !== 0)


            this.otpTextInput[index - 1].focus();
    }

    focusNext(index, value) {
        if (index < this.otpTextInput.length - 1 && value) {
            this.otpTextInput[index + 1].focus();
        }
        if (index === this.otpTextInput.length - 1) {
            this.otpTextInput[index].blur();
        }
        const otp = this.state.otp;
        otp[index] = value;
        this.setState({ otp });
    }

    showSnackBar = (message) => {
        Snackbar.show({
            text: message,
            duration: 2000
        })
    }

    navigateUser = async () => {
        if (this.state.otp.length === 4) {
            await this.setState({ isLoading: true })
            let response = await this.validateOtp();
            let result = await response.json();
            if (result.data.otp) {
                await this.setState({ isLoading: false })
                await this.props.navigation.navigate('newPassword');
            }
            else {
                await this.setState({ isLoading: false })
                this.showSnackBar('Invalid Code');
            }
        }
        else {
            this.showSnackBar('Enter 4 Digit  code');
        }
    }

    validateOtp = async () => {
        let data = this.props.navigation.getParam('name');
        let user_email = this.props.navigation.getParam('email');
        if (this.state.otp.length < 4) {
            this.showSnackBar('Please Enter OTP');
        }
        else {
            let result = await fetch('http://168.235.81.165/cardrive/public/api/user/verify-otp', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    otp: this.state.otp.join('')
                })
            });
            let res = await result.json();
            console.log(res);
            if (res.success === true) {
                this.showSnackBar('Otp matched successfully');
                if (data === 'forgot') {
                    this.props.navigation.navigate('forgotPass2', { email: user_email });
                }
                else {
                    this.props.navigation.navigate('login');
                }

            }
            else {
                this.showSnackBar(result.data.email[0]);
            }
        }


    }
  

    renderInputs() {
        const inputs = Array(4).fill(0);
        const txt = inputs.map((i, j) =>
            
            <TextInput
                placeholder='1'
                style={styles.otp}
                keyboardType="numeric"
                onChangeText={v => this.focusNext(j, v)}
                onKeyPress={e => this.focusPrevious(e.nativeEvent.key, j)}
                ref={ref => this.otpTextInput[j] = ref}
            />
        );
        return txt;
    }

    renderActivityIndicater = () => {
        if (this.state.isLoading) {
            return (
                <ActivityIndicator size="large" color="#4ABEA1" />
            )
        }
        else {
            return (
                <AppButton title="Verify" onPress={() => this.navigateUser()} />
            )
        }
    }
    render() {
        return (
            <View style={baseStyles.appContainer}>
                <Text style={baseStyles.heading}>{this.props.lang.enter_otp}</Text>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    {this.renderInputs()}
                </View>

                <View style={{ alignItems: 'flex-end' }}>
                    <AppButton
                        title={this.props.lang.continue}
                        btnStyle={{
                            borderRadius: radius.xxl,
                            borderTopLeftRadius: 0,
                            paddingHorizontal: padding.xxl
                        }}
                        onPress={() => {
                            this.validateOtp();
                        }}
                    />
                </View>
            </View>
        );
    }
}


const styles = {
    otp: {
        borderWidth: 1,
        borderColor: 'white',
        width: 50,
        height: 50,
        textAlign: 'center',
        borderRadius: 5,
        color: 'white',
        fontSize: 30,
        padding: 1
    }
}

const mapStateToProp = state => {
    return {
        lang: state.staticLang.userLanguage
    }
}

export default connect(mapStateToProp)(OtpScreen);
