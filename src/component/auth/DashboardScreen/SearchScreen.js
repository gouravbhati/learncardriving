import React, { Component } from 'react';
import { FlatList, Modal, PermissionsAndroid, ActivityIndicator, Text, Button, Image, TouchableHighlight } from 'react-native';
import { Segment, View, Item, Input, Icon, Picker, Label } from 'native-base';
import { colors, baseStyles, radius, padding, fonts, margin } from '../../../styles/baseStyle';
import AppButton from '../../utility/AppButton';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import Snackbar from 'react-native-snackbar';
import MapView, { Marker } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import customeMapStyle from '../../utility/MapStyle.json';
import searchQueary from '../../utility/api utility/searchInstructure';
import { Add_Search_Data } from '../../../store/reducer/actions/search_instruct';
import Activity from '../../utility/activity_indicator';

class SearchScreen extends Component {
    state = {
        activePage: 1,
        licence: 'government',
        name: '',
        lat: 22.7533,
        long: 75.8937,
        token: '',
        placename: '',
        isloading: false,
        model: false,
        current_lat: '',
        current_lng: '',
        trainer_plac: this.props.lang.search_by_trainer_name,
        active: false
    }

    selectComponent = (activePage) => () => this.setState({ activePage })

    componentDidMount = async () => {
        this.getLocationPermmision();
        await Geolocation.getCurrentPosition(info => {
            this.setState({ lat: parseFloat(info.coords.latitude) });
            this.setState({ long: parseFloat(info.coords.longitude) });
            this.setState({ current_lat: parseFloat(info.coords.latitude) });
            this.setState({ current_lng: parseFloat(info.coords.longitude) });
            this.geo(parseFloat(info.coords.latitude), parseFloat(info.coords.longitude));
        }, (error) => { },
            { enableHighAccuracy: true, timeout: 10000, maximumAge: 3000 })
        this.setState({ token: await AsyncStorage.getItem('TOKEN') });
    }


    geo = async (lat, long) => {
        this.setState({ isloading: true });
        await fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + lat + ',' + long + '&key=' + 'AIzaSyBcqbBk-OzNsQ7gBGeYlXfLdnIirFPbkRE')
            .then((response) => response.json())
            .then((responseJson) => {
                let result = responseJson.results[0].address_components.map(value => value.long_name)
                this.setState({ placename: result.join(" ") });
                this.setState({ isloading: false });
            });
    }

    getLocationPermmision = () => {
        try {
            PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
        } catch (err) {
            console.warn(err);
        }
    }

    showPlaceName = () => {
        if (this.state.isloading)
            return (
                <ActivityIndicator color='black' />
            )
        else {
            console.log(this.state.placename);
            return (<View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Icon name="locate" style={{ paddingRight: padding.md }} />
                <Text>{this.state.placename}</Text>
            </View>)
        }
    }

    updateregion = async (region) => {
        this.setState({ lat: region.latitude, long: region.longitude });
        await this.geo(region.latitude, region.longitude);
        this.setState({ isloading: false });
    }

    checklocation = () => {
        if (this.state.lat != null && this.state.long != null) {
            this.setState({ model: false });
        }
    }


    showLocationPicker = () => {
        return (
            <Modal visible={this.state.model} >
                <View style={{ flex: 1, position: 'relative' }}>
                    <Image
                        style={{ height: 50, width: 50, resizeMode: 'contain', position: 'absolute', top: '40%', zIndex: 2, left: '45%' }}
                        source={require('../../../assets/geo_loc.png')}
                    />
                    <MapView style={{ flex: 4 }}
                        customMapStyle={customeMapStyle}
                        onRegionChangeComplete={(region) => this.updateregion(region)}
                        showsUserLocation={true}
                        showsMyLocationButton={true}
                        initialRegion={{
                            latitude: this.state.lat,
                            longitude: this.state.long,
                            latitudeDelta: 0.03,
                            longitudeDelta: 0.03
                        }}
                    >

                    </MapView>
                    <View style={Styles.mapView} >
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={Styles.heading2} >Move map for location</Text>
                        </View>
                        <this.showPlaceName />
                        <AppButton title='pick location'
                            btnStyle={{
                                marginVertical: margin.md,
                                borderRadius: radius.xxl,
                                borderTopLeftRadius: 0,
                                paddingHorizontal: padding.xxl,
                            }}
                            onPress={() => this.checklocation()} />
                    </View>
                </View>
            </Modal>
        )
    }

    searchQuearyForInstructure = async () => {
        this.setState({ active: true });
        let response = await searchQueary(this.state.name, 22.7533, 75.8937, this.state.licence, this.state.activePage == 1 ? 'wemale' : 'male', this.state.token, 'en');
        if (response.success) {
            console.log(response)
            if (response.data.length == 0) {
                Snackbar.show({
                    text: 'No Result Found',
                    duration: 2000
                })
                this.setState({ active: false });
            }
            else {
                this.setState({ active: false });
                this.props.Add_Search_Data(response.data);
                this.props.navigation.navigate('traneeList', { 'type': 'search' });
            }
        }
    }

    render() {
        return (
            <View style={[baseStyles.appContainer, baseStyles.justifyContentStart]}>
                <this.showLocationPicker />
                <Activity show={this.state.active} />
                <Item rounded style={{ marginBottom: margin.xl }}>
                    <Input
                        placeholder={this.props.lang.search_by_location}
                        placeholderTextColor="#fff"
                        style={{ paddingLeft: padding.xl, color: colors.white }}
                        onFocus={() => this.setState({ model: true })}
                    />
                    <Icon name="ios-search" style={{ color: colors.white, paddingRight: padding.lg }} />
                </Item>
                <Item rounded style={{ marginBottom: margin.xl }}>
                    <Input
                        placeholder={this.state.trainer_plac}
                        value={this.state.name}
                        placeholderTextColor="#fff"
                        style={{ paddingLeft: padding.xl, color: colors.white }}
                        onChangeText={(name) => this.setState({ name })}
                        onFocus={() => this.setState({ trainer_plac: '' })}
                        onEndEditing={() => this.setState({ trainer_plac: this.props.lang.search_by_trainer_name })}
                    />

                    <Icon name="ios-search" style={{ color: colors.white, paddingRight: padding.lg }} />
                </Item>
                <Item rounded style={{ marginBottom: margin.xl, paddingLeft: padding.lg }}>
                    <Picker
                        selectedValue={this.state.licence}
                        mode='dropdown'
                        itemStyle={{ height: 500 }}
                        style={{ height: 50, width: 200, color: 'white', fontSize: 14 }}
                        onValueChange={(item) => this.setState({ licence: item })}
                    >
                        <Picker.Item label={this.props.lang.personal} value={this.props.lang.personal} />
                        <Picker.Item label={this.props.lang.government} value={this.props.lang.government} />
                    </Picker>

                    <Icon name="arrow-down" style={{ color: colors.white, paddingRight: padding.lg }} />
                </Item>
                <Text style={Styles.heading}>{this.props.lang.gender}</Text>
                <Segment style={Styles.sagmentStyle}>
                    <AppButton active={this.state.activePage === 1}
                        onPress={this.selectComponent(1)}
                        btnStyle={{
                            backgroundColor: this.state.activePage === 1 ? "#FFE400" : "#000", marginRight: margin.sm, marginVertical: 0, borderRadius: radius.xxl,
                            borderTopLeftRadius: 0,
                        }}
                        titleStyle={{ color: this.state.activePage === 1 ? "#000" : "#fff" }}
                        title={this.props.lang.women_instructor} />

                    <AppButton active={this.state.activePage === 2}
                        onPress={this.selectComponent(2)}
                        btnStyle={{
                            backgroundColor: this.state.activePage === 1 ? "#000" : "#FFE400", marginRight: margin.sm, marginVertical: 0, borderRadius: radius.xxl,
                            borderTopLeftRadius: 0,
                        }}
                        titleStyle={{ color: this.state.activePage === 1 ? "#fff" : "#000" }}
                        title={this.props.lang.men_instructor} />

                </Segment>
                <View padder>
                </View>
                <Text style={{ color: 'white' }}>{this.state.placename}</Text>

                <View style={{ justifyContent: 'flex-end', flex: 1 }}>
                    <AppButton
                        title={this.props.lang.search}
                        btnStyle={{
                            borderRadius: radius.xxl,
                            borderTopLeftRadius: 0,
                            paddingHorizontal: padding.xxl
                        }}
                        onPress={() => this.searchQuearyForInstructure()}
                    />
                </View>
            </View>
        )
    }
}

const Styles = {
    btnStyle: {
        borderRadius: radius.xxl,
        borderTopLeftRadius: 0,
        margin: 'auto',
        alignSelf: 'center',
    },
    sagmentStyle: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: colors.black,
        paddingHorizontal: padding.xl
    },
    heading: {
        fontSize: fonts.lg,
        fontWeight: 'bold',
        color: colors.white,
        marginBottom: margin.lg
    },
    heading2: {
        fontSize: fonts.xl,
        fontWeight: 'bold',
        color: colors.black,
        marginBottom: margin.sm,
        marginRight: margin.lg
    },
    mapView: {
        flex: 1,
        backgroundColor: colors.white,
        padding: padding.lg,
        borderTopLeftRadius: radius.xl,
        borderTopRightRadius: radius.xl,
        marginTop: -20
    }
}


const mapStateToProps = (state) => {
    return {
        lang: state.staticLang.userLanguage,
    }
}

export default connect(mapStateToProps, { Add_Search_Data })(SearchScreen);
