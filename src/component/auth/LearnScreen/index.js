import React from 'react';
import { Image } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation';
import LearnDashBoard from '../LearnScreen/LearnDashboard';
import TrainingDetail from '../LearnScreen/TrainingDetail';
import { colors, fonts} from '../../../styles/baseStyle';
import SucessTraning from './SucessTraning';
import HeaderTitle from '../../../static language/staticlangHeader';
const AppTabNavigator = createStackNavigator({

    trainers: {
        screen: TrainingDetail,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />),
            headerTitle: <HeaderTitle title={"Training Detail"} />,
            headerTitleStyle: { color: colors.white, fontSize: fonts.xl, fontWeight: 'bold' },

        }

    },
    learnDash: {
        screen: LearnDashBoard,
        navigationOptions: {
            headerBackImage: (() => <Image source={require('../../../assets/notification.png')} style={Styles.backImg} />),
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />),
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerTitle: <HeaderTitle title={"Learn"} />,
            headerTitleStyle: { color: colors.white, fontSize: fonts.xl, fontWeight: 'bold' },
        }
    },

    sucessTranee: {
        screen: SucessTraning,
        navigationOptions: {
            headerBackImage: (() => <Image source={require('../../../assets/notification.png')} style={Styles.backImg} />),
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />),
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerTitle: <HeaderTitle title={"Learn"} />,
            headerTitleStyle: { color: colors.white, fontSize: fonts.xl, fontWeight: 'bold' },
        }
    },
},
    {
        initialRouteName: "learnDash",
        headerBackTitleVisible: false,
        headerLayoutPreset:'center'
    }


);
const LearnRoot = createAppContainer(AppTabNavigator)



const Styles = {
    backImg: {
        width: 25,
        height: 20,
        resizeMode: 'contain'
    }
}
export default LearnRoot;