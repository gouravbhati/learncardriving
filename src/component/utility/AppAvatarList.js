import React from 'react';
import { Text, ListItem, Left, Thumbnail, Body } from 'native-base'
import {colors,radius,padding} from '../../styles/baseStyle';

const AppAvatraList = ({ source, name, hireing, price,container,onPress }) => {
    return (
        <ListItem avatar noBorder style={[Styles.container,container]} onPress={onPress}>
            <Left>
                <Thumbnail large source={source} style={{ borderRadius: radius.md }} />
            </Left>
            <Body>
                <Text style={Styles.nameStyle}>{name}</Text>
                <Text style={{ color: colors.white }}>Hired on :{hireing}</Text>
                <Text style={{ color: colors.white }}>Hourly Price -{price}</Text>
            </Body>
        </ListItem>
    );
}
const Styles ={
    nameStyle:{
        color: colors.white, 
        fontSize: 24, 
        fontWeight: 'bold',
        paddingBottom:padding.sm
    },
    container:{
        marginLeft:0,
        marginRight:0
    }
}


export default AppAvatraList;