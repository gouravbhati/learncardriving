import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, ScrollView, TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';
import { baseStyles, colors, fonts, margin } from '../../../styles/baseStyle';
import { Icon } from 'native-base';
import { setStaticLangArabic, setStaticLangEnglish } from '../../../store/reducer/actions/staticLanguageReduceraction';
import AsyncStorage from '@react-native-community/async-storage';

class HomeScreen extends Component {

    state = {
        selectedlangiseng: true
    }

    componentDidMount = async () => {
        let lang = await AsyncStorage.getItem('LANG');
        if (lang == 'ar') {
            this.props.setStaticLangArabic();
            this.setState({ selectedlangiseng: false });
        }
        else {
            this.props.setStaticLangEnglish();
        }
    }


    switchLanguage = () => {
        return (
            <View style={{ flexDirection: 'row' }} >
                <Text style={{ color: 'white', }} >Language :</Text>
                <TouchableWithoutFeedback onPress={() => this.setLanguageEnglish()} >
                    <Text style={{ color: this.state.selectedlangiseng ? 'yellow' : 'white' }} > English </Text>
                </TouchableWithoutFeedback>
                <Text style={{ color: 'white' }} > /</Text>
                <TouchableWithoutFeedback onPress={() => this.setLanguageArabic()} >
                    <Text style={{ color: !this.state.selectedlangiseng ? 'yellow' : 'white' }} >عربى</Text>
                </TouchableWithoutFeedback>
            </View>
        )
    }



    setLanguageArabic = async () => {
        this.setState({ selectedlangiseng: false });
        this.props.setStaticLangArabic();
        await AsyncStorage.setItem('LANG', 'ar');
    }

    setLanguageEnglish = async () => {
        this.setState({ selectedlangiseng: true });
        this.props.setStaticLangEnglish();
        await AsyncStorage.setItem('LANG', 'en');
    }


    render() {
        return (
            <ScrollView>
                <View style={baseStyles.appContainer}>
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
                        <this.switchLanguage />
                    </View>
                    <Text style={baseStyles.heading}>{this.props.lang.Hello}!</Text>
                    <Text style={{ color: colors.white, fontSize: fonts.mld }}>{this.props.lang.message}.</Text>
                    <TouchableOpacity
                        onPress={() => this.props.navigation.navigate('login')}
                        style={baseStyles.alignItemsCenter}>
                        <Image
                            style={{ height: 300, width: 300, resizeMode: 'contain' }}
                            source={ this.state.selectedlangiseng ? require('../../../assets/iconBtn.png') : require('../../../assets/iconBtn2.png') } />
                    </TouchableOpacity>

                    <View style={baseStyles.alignItemsCenter}>
                        <Text style={baseStyles.subHeading}>{this.props.lang.account}?</Text>
                        <TouchableOpacity
                            style={[baseStyles.flexRow, baseStyles.alignItemsCenter, baseStyles.marginTopMd]}
                            onPress={() => this.props.navigation.navigate('signUp')}
                        >
                            <Icon name="ios-person-add" style={{ color: colors.yellow }} />
                            <Text style={{ color: colors.yellow, fontSize: fonts.xl, marginLeft: margin.md }}>{this.props.lang.signup}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

const mapStateToProp = state => {
    return {
        lang: state.staticLang.userLanguage
    }
}

export default connect(mapStateToProp, { setStaticLangArabic, setStaticLangEnglish })(HomeScreen);

