import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, StyleSheet, View } from 'react-native';

class HeaderTitle extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        switch (this.props.title) {
            case 'trainer':
                return <Text style={{ color: 'white', textAlign: 'center' }} >{this.props.lang.trainers}</Text>
            case 'learn':
                return <Text style={{ color: 'white', textAlign: 'center' }} >{this.props.lang.learn}</Text>
            case 'Notification':
                return <Text style={style.headerStyle} >{this.props.lang.notification}</Text>
            case 'Explore':
                return <Text style={style.headerStyle} >{this.props.lang.explore}</Text>
            case 'Search':
                return <Text style={style.headerStyle} >{this.props.lang.search}</Text>
            case 'Instructor Detail':
                return <Text style={style.headerStyle} >{this.props.lang.instructor_detail}</Text>
            case 'Payment Method':
                return <Text style={style.headerStyle} >{this.props.lang.pay}</Text>
            case 'Review':
                return <Text style={style.headerStyle} >{this.props.lang.review}</Text>
            case 'All Trainers':
                return <Text style={style.headerStyle} >{this.props.lang.review}</Text>
            case 'Book Instructor':
                return <Text style={style.headerStyle} >{this.props.lang.book_instructor}</Text>
            /// not in center
            case 'Profile':
                return <Text style={style.headerStyle} >{this.props.lang.profile}</Text>
            case 'Payment History':
                return <Text style={style.headerStyle, { alignSelf: 'center' }} >{this.props.lang.payment_history}</Text>
            case 'Change Password':
                return <Text style={style.headerStyle} >{this.props.lang.change_pass}</Text>
            case 'Account Information':
                return <Text style={style.headerStyle} >{this.props.lang.account_info}</Text>
            case 'Language Setting':
                return <Text style={style.headerStyle} >{this.props.lang.lanuage_setting}</Text>
            case 'Learn':
                return <Text style={style.headerStyle} >{this.props.lang.learn}</Text>
            case 'Training Detail':
                return <Text style={style.headerStyle} >{this.props.lang.training_detail}</Text>
            default:
                return <Text style={{ color: 'white', textAlign: 'center' }} >No header</Text>
        }
    }
}

const style = StyleSheet.create({
    headerStyle: {
        fontSize: 22,
        color: 'white',
        //textAlign: 'center',
    }
})



const mapStateToProps = state => {
    return {
        lang: state.staticLang.userLanguage
    }
}

export default connect(mapStateToProps)(HeaderTitle);