import React, { Component } from 'react';
import { Text, Picker } from 'react-native';
import { Item, Input, View, Label } from 'native-base'
import { radius, margin, colors, padding, fonts } from '../../styles/baseStyle';
import phonejson from '../utility/code.json';

const phone = [
    {
        "name": "Argentina",
        "dial_code": "+54",
        "code": "AR"
    },
    {
        "name": "Aruba",
        "dial_code": "+297",
        "code": "AW"
    },
    {
        "name": "Bahamas",
        "dial_code": "+1 242",
        "code": "BS"
    },
    {
        "name": "Bahrain",
        "dial_code": "+973",
        "code": "BH"
    },

];

function RenderPicker() {
    return <Picker.Item label='hello' value='hrelo' />
}

const AppInput = ({
    placeholder,
    keyboardType,
    onKeyPress,
    onChangeText,
    errorMsg,
    value,
    label,
    inputStyle,
    secureTextEntry,
    selectedValue,
    onValueChange,
    showpickList
}) => {
    return (
        <View style={{ borderColor: 'white', borderWidth: 1, borderRadius: 9, paddingTop: 5, alignItems: 'center' }} >
            <Label style={Styles.floatingStyle}>{label}</Label>
            <View style={{ height: 60, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }} >
                    <Picker
                        selectedValue={'05'}
                        mode='dropdown'
                        itemStyle={{ height: 500 }}
                        style={{ height: 50, width: 200, color: 'white', fontSize: 14 }}
                    >
                        {
                            phone.map((items, index) => <Picker.item label={`${items.code} ${items.dial_code}`} value='+91' />)
                        }
                    </Picker>
                <Input
                    placeholder={placeholder}
                    placeholderTextColor="#fff"
                    value={value}
                    error={errorMsg}
                    keyboardType={keyboardType}
                    onKeyPress={onKeyPress}
                    onChangeText={onChangeText}
                    style={Styles.textStyle}
                    secureTextEntry={secureTextEntry}
                />
            </View>
        </View >
    );
}
const Styles = {
    inputStyle: {
        borderRadius: radius.xl,
        marginVertical: margin.md,
        height: 55,
        alignSelf: 'center',
    },
    textStyle: {
        textAlign: 'center',
        textTransform: 'capitalize',
        color: colors.white,
        fontWeight: 'bold'
    },
    floatingStyle: {
        color: colors.yellow,
        //textTransform: 'capitalize',
        textAlign: 'center',
        position: 'absolute',
        top: -5,
        paddingLeft: margin.lg,
        fontWeight: 'bold',
        paddingRight: margin.lg,
        alignSelf: 'center',
        backgroundColor: colors.black,

    }
}

export default AppInput;