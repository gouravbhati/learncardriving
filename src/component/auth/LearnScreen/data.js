export const calList = [
    {
        day: "Monday",
        date: "1 Nov 2019",
        time: "11:00 - 12:00pm",
    },
    {
        day: "Tuesday",
        date: "2 Nov 2019",
        time: "10:00 - 11:00pm",
    },
    {
        day: "Wednesday",
        date: "5 Nov 2019",
        time: "8:00 - 10:00pm",
    },
    {
        day: "Thursday",
        date: "8 Nov 2019",
        time: "11:00 - 12:00pm",
    },
];

