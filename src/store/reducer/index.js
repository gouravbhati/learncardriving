import { combineReducers } from 'redux';
import RootRedducer from './rootReducer';
import staticlanguagereducer from './staticlangreducer';
import Instructor_detail from './instructor_reducer';
import Notification from './notification_reducer';
import OngoingReducer from './onGoingReducer';
import searchInstructure from './search_instruct';

export default combineReducers({
  rootReducer: RootRedducer,
  staticLang: staticlanguagereducer,
  instructor_detail: Instructor_detail,
  notification: Notification,
  ongoing: OngoingReducer,
  search_instructor: searchInstructure
});