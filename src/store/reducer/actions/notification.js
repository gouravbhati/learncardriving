export const Add_Notification=(data)=>{
    return {
        type: 'ADD',
        payload: data
    }
}

export const Remove_Notification=(data)=>{
    return {
        type: 'REMOVE',
        payload: data
    }
}