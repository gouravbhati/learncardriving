
const istate = {
    notification: [],
}

export default (state = istate, action) => {
    let n = parseInt(state.flag);
    switch (action.type) {
        case 'ADD':
            return {
                ...state,
                notification: action.payload,
            }
        default: {
            return state;
        }
    }
}