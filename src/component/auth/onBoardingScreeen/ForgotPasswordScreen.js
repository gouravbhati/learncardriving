import React, { Component } from 'react';
import { View, Text, SegmentedControlIOSComponent } from 'react-native';
import { connect } from 'react-redux';
import { baseStyles, radius, padding } from '../../../styles/baseStyle';
import AppInput from '../../utility/AppInput';
import AppButton from '../../utility/AppButton';
import Activity from '../../utility/activity_indicator';
import Snackbar from 'react-native-snackbar';

class ForgotPasswordScreen extends Component {
    state = {
        email: '',
        email_er: '',
        email_plac: this.props.lang.email_placholder,
        activity: false
    }
    componentDidMount() {

    }
    validate() {
        if (this.state.email === '') {
            this.showSnackBar('Email is Required');
            return 0
        }
        else {
            this.setState({ activity: true });
            this.sendOtp();
        }
    }

    showSnackBar = (message) => {
        Snackbar.show({
            text: message,
            duration: 2000
        })
    }
    sendOtp = async () => {
        try {
            let res = await fetch('http://168.235.81.165/cardrive/public/api/user/forgot-password', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email: this.state.email,
                })
            });
            let result = await res.json();
            this.setState({ activity: false })
            if (result.success === true) {
                alert(result.data.otp);
                this.props.navigation.navigate('otp', { name: 'forgot', email: this.state.email });
            }
        }
        catch (error) {
            this.setState({ activity: false })
            this.setState({ activity: false });
            if (error.message == 'Network request failed')
                Snackbar.show({
                    text: this.props.lang.internet_error,
                    duration: 4000
                })
            else {
                Snackbar.show({
                    text: this.props.lang.email_error,
                    duration: 4000
                })
            }
        }

    }
    render() {
        return (
            <View style={baseStyles.appContainer}>
                <Activity show={this.state.activity} />
                <Text style={baseStyles.heading}>{this.props.lang.forgot_password}</Text>
                <AppInput
                    label={this.props.lang.email}
                    value={this.state.email}
                    placeholder={this.state.email_plac}
                    onFocus={() => this.setState({ email_plac: '' })}
                    keyboardType='email-address'
                    onEndEditing={() => this.setState({ email_plac: this.props.lang.email_placholder })}
                    onChangeText={(text) => { this.setState({ email: text }) }}
                />

                <View style={{ alignItems: 'flex-end' }}>
                    <AppButton
                        title={this.props.lang.continue}
                        btnStyle={{
                            borderRadius: radius.xxl,
                            borderTopLeftRadius: 0,
                            paddingHorizontal: padding.xxl
                        }}
                        onPress={() => {
                            this.validate();

                        }}
                    />
                </View>
            </View>
        );
    }
}


const mapStateToProp = state => {
    return {
        lang: state.staticLang.userLanguage
    }
}

export default connect(mapStateToProp)(ForgotPasswordScreen);

