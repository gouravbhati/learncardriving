import React, { Component } from 'react';
import { View, ActivityIndicator } from 'react-native'
import { colors, margin, fonts, baseStyles, padding } from '../../../../styles/baseStyle';
import AppInput from '../../../utility/AppInput';
import AppButton from '../../../utility/AppButton';
import {connect} from 'react-redux';
import Snackbar from 'react-native-snackbar';

class AccountInfo extends Component {
    state={
        activity: false,
        name: '',
        email: '',
    }
    change_password(){
        this.setState({ activity: true })
            setTimeout(() => {
                this.setState({ activity: false })
                Snackbar.show({
                    text: 'Account Detail Changed Successfully',
                    duration: 2000
                });

            }, 5000);
    }
    activity_indicator() {
        if (this.state.activity === true) {
            return <ActivityIndicator size="large" color="#ffe200" />
        }
        else {
            return null;
        }
    }
    render() {
        return (
            <View style={[baseStyles.appContainer, Styles.container]}>
                <View style={Styles.innerContainer}>
                    <AppInput label={this.props.lang.name} />
                    <AppInput label={this.props.lang.email} />
                </View>
            {this.activity_indicator()}
              <View style={{justifyContent:'flex-end',flex:1}}>
                <AppButton title={this.props.lang.save_change}
                    onPress={()=>{this.change_password()}}
                />
                </View>
            </View>
        );
    }
}

const Styles = {

    container: {
        paddingHorizontal: 0,
        justifyContent: 'flex-start',

    },
    listItemStyle: {
         backgroundColor: colors.offBlack,
        marginBottom: margin.sm,
        height: 60,
        paddingLeft: 30
    },
    innerContainer:
    {
        backgroundColor: colors.offBlack,
        paddingHorizontal: padding.lg,
        paddingVertical:padding.xl

    },
    listText: {
        color: colors.white,
        fontSize: fonts.xl
    },
}

const mapstateToProps=(state)=>{
    return{
        lang: state.staticLang.userLanguage
    }
}
export default connect(mapstateToProps)(AccountInfo);