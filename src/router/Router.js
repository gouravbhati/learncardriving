import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import LoginRoot from '../component/auth/onBoardingScreeen';
import AppRoot from '../component/auth/DashboardScreen';
import ProfileRoot from '../component/auth/OtherScreen';
import HeaderTitle from '../static language/staticlangHeader';

const AppDashboard = createStackNavigator({
    learn: {
        screen: AppRoot,
        header: null,
    },
    profile: {
        screen: ProfileRoot,
        navigationOptions: {
            header : null,
        }
    }
},
    {
        initialRouteName: 'learn',
        headerMode: 'none'
    }
);

const RootNavigation = createSwitchNavigator({
    Auth: LoginRoot,
    Dashboard: AppDashboard
})

const AppRootNavigation = createAppContainer(RootNavigation)

export default AppRootNavigation;