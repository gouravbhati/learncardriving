import React, { Component } from 'react';
import { Image, TouchableOpacity, Modal, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { View, Text, Thumbnail, Icon } from 'native-base';
import { baseStyles, fonts, colors, margin, padding, radius } from '../../../styles/baseStyle';
import AppButton from '../../utility/AppButton';
import { connect } from 'react-redux';
import MapView, { Marker } from 'react-native-maps';
import { ScrollView } from 'react-native-gesture-handler';
import RazorpayCheckout from 'react-native-razorpay';
import { bookslot } from '../../utility/api utility/apis';
import Geolocation from '@react-native-community/geolocation';
import customeMapStyle from '../../utility/MapStyle.json';
import Snackbar from 'react-native-snackbar';
import fetchUserData from '../../utility/api utility/fetchuserdatabyapi';
import { booked_instructur } from '../../utility/api utility/apis';
import { setBookedInstructure } from '../../../store/reducer/actions/rootReducerAction'

class ReviewScreen extends Component {

    state = {
        slot_id: '',
        donate_amount: '',
        totalclass: '',
        token: '',
        isloading: false,
        placename: '',
        key: 0,
        amount: '',
        transection_id: '',
        token: '',
        driver_place: 'pick a location',
        instructure_location: '',
        lat: '',
        long: '',
        model: false
    }

    componentDidMount = async () => {

        await Geolocation.getCurrentPosition(info => {
            this.setState({ lat: parseFloat(info.coords.latitude) });
            this.setState({ long: parseFloat(info.coords.longitude) });
        }, (error) => { this.setState({ lat: 23.554, long: 54.56546 }) }),
            { enableHighAccuracy: true, timeout: 10000, maximumAge: 3000 }
        this.setState({ token: await AsyncStorage.getItem('TOKEN') });
        this.setState({ amount: this.collectPriceByTime(this.props.navigation.getParam('time'), this.props.data.hourly_rate, this.props.navigation.getParam('selectedclasses')) });
        this.setState({ slot_id: this.props.navigation.getParam('slot_id') });
        this.setState({ totalclass: this.props.navigation.getParam('selectedclasses') });
        console.log(await this.geo(this.props.data.lat, this.props.data.lng));
    }

    collectPriceByTime = (time, instructureprice, days) => {
        let newTime = time.split(':');
        let price = (newTime[0] * instructureprice) + ((newTime[1] / 60) * instructureprice)
        return (price * days);
    }

    checklocation = () => {
        if (this.state.lat != null && this.state.long != null) {
            this.setState({ model: false });
            this.setState({ driver_place: this.state.placename });
        }
    }

    updateregion = async (region) => {
        this.setState({ lat: region.latitude, long: region.longitude });
        await this.geo(region.latitude, region.longitude);
        this.setState({ isloading: false });
    }

    geo = async (lat, long) => {
        this.setState({ isloading: true });
        let responsepath = ''
        await fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + lat + ',' + long + '&key=' + 'AIzaSyBcqbBk-OzNsQ7gBGeYlXfLdnIirFPbkRE')
            .then((response) => response.json())
            .then((responseJson) => {
                let result = responseJson.results[0].address_components.map(value => value.long_name)
                this.setState({ placename: result.join(" ") });
                this.setState({ isloading: false });
                responsepath = result.join("");

                if (this.state.instructure_location == '') {
                    this.setState({ instructure_location: result.join(" ") });
                }
            });
        return responsepath
    }


    paymentCheck = () => {
        var options = {
            description: 'Credits towards consultation',
            image: 'https://i.imgur.com/3g7nmJC.png',
            currency: 'INR',
            key: 'rzp_test_fRwUUu9SG0zYBk',
            amount: '50000',
            external: {
                wallets: ['paytm']
            },
            name: 'Ravi',
            prefill: {
                email: 'gourav@gmail.com',
                contact: '123456789',
                name: 'Ravi Gahlot',
            },
            theme: { color: '#F37254' }
        }
        RazorpayCheckout.open(options).then((data) => {
            // handle success
            this.makeApiRequest(data.razorpay_payment_id);
        }).catch((error) => {
            // handle failure
            alert(`Error: ${error.code} | ${error.description}`);
        });
        RazorpayCheckout.onExternalWalletSelection(data => {
            alert(`External Wallet Selected: ${data.external_wallet} `);
        });
    }

    makeApiRequest = async (id) => {
        let response = await fetch(bookslot, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${this.state.token}`,

            },
            body: JSON.stringify({
                'classes': this.state.totalclass,
                'slot_id': this.state.slot_id,
                'instructor_id': this.props.data.id,
                'address': this.updateDriverAddress(),
                'lat': this.state.key == 0 ? this.props.data.lat : this.state.lat,
                'lng': this.state.key == 1 ? this.props.data.lng : this.state.long,
                'payment_id': id
            })
        });

        let result = await response.json();
        if (result.success) {
            let response = await fetchUserData(booked_instructur, this.state.token, 'en');
            this.props.setBookedInstructure(response.data);
            this.props.navigation.navigate('sucessScreen');
        }
        else {
            Snackbar.show({
                text: 'erro',
                duration: result.data.slot_id[0]
            })
        }
    }

    showPlaceName = () => {
        if (this.state.isloading)
            return (
                <ActivityIndicator color='black' />
            )
        else {
            console.log(this.state.placename);
            return (<View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Icon name="locate" style={{ paddingRight: padding.md }} />
                <Text>{this.state.placename}</Text>
            </View>)
        }
    }


    showModel = () => {
        return (
            <Modal
                visible={this.state.model}
            >

                <View style={{ flex: 1, position: 'relative' }}>
                    <Image
                        style={{ height: 50, width: 50, resizeMode: 'contain', position: 'absolute', top: '40%', zIndex: 2, left: '45%' }}
                        source={require('../../../assets/geo_loc.png')}
                    />

                    <MapView style={{ flex: 4 }}
                        customMapStyle={customeMapStyle}
                        onRegionChangeComplete={(region) => this.updateregion(region)}
                        showsUserLocation={true}
                        showsMyLocationButton={true}
                        initialRegion={{
                            latitude: this.state.lat,
                            longitude: this.state.long,
                            latitudeDelta: 0.03,
                            longitudeDelta: 0.03
                        }}
                    >
                    </MapView>
                    <View style={Styles.mapView} >
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={Styles.heading2} >Move map for location</Text>
                        </View>
                        <this.showPlaceName />
                        <AppButton title='pick location'
                            btnStyle={{
                                marginVertical: margin.md,
                                borderRadius: radius.xxl,
                                borderTopLeftRadius: 0,
                                paddingHorizontal: padding.xxl,
                            }}
                            onPress={() => this.checklocation()} />
                    </View>

                </View>

            </Modal>
        )
    }

    updateDriverAddress = () => {
        if (this.state.key == 0) {
            return this.state.instructure_location
        }
        else {
            return this.state.driver_place
        }
    }

    navigateToPay = () => {
        if (this.state.key == 1)
            if (this.state.driver_place == 'pick a location')
                Snackbar.show({
                    text: 'Select A location ',
                    duration: 2000
                });
            else
                this.paymentCheck();
        else
            this.paymentCheck();
    }


    render() {
        return (
            <View style={[baseStyles.appContainer, Styles.container]}>
                <this.showModel />
                <ScrollView>
                    <Text style={[Styles.textBold, baseStyles.paddingVerticalLg, baseStyles.paddingLeftLg]}>{this.props.lang.instructor_fees}</Text>
                    <View style={[baseStyles.media, Styles.content]}>
                        <Image source={require('../../../assets/review_img.png')}
                            style={Styles.imgStyle} />
                        <View style={[baseStyles.mediaBody]}>
                            <Text style={[Styles.text, baseStyles.textAlignRight]}>{this.props.lang.fee}</Text>
                            <Text style={[Styles.textBold, baseStyles.textAlignRight]}>{this.props.lang.bhd} {this.props.data.hourly_rate}/ hr</Text>
                            <Text style={[Styles.textBold, baseStyles.textAlignRight]}>{this.props.lang.amount}</Text>
                        </View>
                    </View>

                    <View style={{ paddingHorizontal: padding.lg }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: margin.lg }}>
                            <Text style={Styles.heading}>{this.props.lang.instructor_detail}</Text>
                        </View>
                        <View style={[baseStyles.media]}>
                            <TouchableOpacity style={{ marginRight: margin.lg }} onPress={() => this.props.navigation.navigate('instructorDetail')} >
                                <Thumbnail source={{ uri: this.props.data.profile_url }} style={Styles.userImg} />
                            </TouchableOpacity>
                            <View style={[baseStyles.mediaBody]}>
                                <Text style={Styles.nameStyle}>{this.props.data.name} </Text>
                                <Text style={Styles.text}>{this.props.data.year_of_experience} Yrs of Experience</Text>
                                <Text style={Styles.text}> {this.props.data.city},{this.props.data.state} </Text>
                                <Text style={Styles.text}>{this.props.data.licence_type} Licence Trainer </Text>
                            </View>

                        </View>
                    </View>
                    <View style={[baseStyles.media, Styles.content, baseStyles.paddingVerticalLg, baseStyles.marginVerticalLg]}>

                        <Text style={Styles.text}>{this.props.lang.no_of_classes}</Text>
                        <Text style={Styles.btnStyle}>{this.state.totalclass}</Text>
                    </View>

                    <View style={baseStyles.paddingHorizontalMd}>
                        <Text style={[Styles.heading, baseStyles.paddingVerticalLg, baseStyles]}>{this.props.lang.training_location_pre}</Text>

                        <View>
                            <View style={[baseStyles.marginBottomMd, baseStyles.paddingTopLg]}>
                                <TouchableOpacity style={Styles.btn} onPress={() => this.setState({ key: 0 })} >
                                    <Image style={Styles.img} source={this.state.key == 1 ? require("../../../assets/radio_circle.png") : require("../../../assets/radio.png")} />
                                    <View style={baseStyles.mediaBody}>
                                        <Text style={Styles.textXl}>{this.props.lang.trainer_loc_place}</Text>
                                        <Text style={Styles.text2}>{this.state.instructure_location}</Text>
                                    </View>
                                </TouchableOpacity>

                                <TouchableOpacity style={Styles.btn} onPress={() => this.setState({ key: 1 })} >
                                    <Image style={Styles.img} source={this.state.key == 1 ? require("../../../assets/radio.png") : require("../../../assets/radio_circle.png")} />
                                    <View style={baseStyles.mediaBody}>
                                        <Text style={Styles.textXl}>{this.props.lang.my_loc_train}</Text>
                                        <TouchableOpacity onPress={() => this.setState({ model: true, key: 1 })} >
                                            <Text style={[Styles.text2, baseStyles.yellowText]}>{this.state.driver_place}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </TouchableOpacity>

                            </View>
                        </View>

                        <View style={{ justifyContent: 'flex-end', flex: 1 }}>
                            <AppButton
                                title={this.props.lang.pay}
                                btnStyle={{
                                    borderRadius: radius.xxl,
                                    borderTopLeftRadius: 0,
                                    paddingHorizontal: padding.xxl
                                }}
                                onPress={() => this.navigateToPay()}
                            />
                        </View>
                    </View>

                </ScrollView>
            </View>
        );
    }
}


const Styles = {
    container: {
        paddingHorizontal: 0,
        justifyContent: 'flex-start'
    },
    content: {
        backgroundColor: colors.offBlack,
        paddingHorizontal: padding.lg,

    },
    imgStyle: {
        height: 170,
        width: 150,
        resizeMode: 'contain'
    },
    text: {
        fontSize: fonts.mld,
        color: colors.white,
    },
    textXl: {
        fontSize: 20,
        color: colors.white,
    },
    userImg: {
        height: 120,
        width: 120,
        borderRadius: radius.lg
    },
    imgRate: {
        height: 20,
        width: 120,
        marginRight: margin.lg
    },
    heading: {
        color: colors.white,
        fontSize: 25,
        fontWeight: 'bold'
    },
    nameStyle: {
        fontSize: fonts.xl,
        fontWeight: 'bold',
        color: colors.white
    },
    textBold: {
        fontWeight: 'bold',
        fontSize: fonts.xl,
        color: colors.white,
        marginTop: margin.md
    },
    btnStyle: {
        padding: padding.md,
        borderWidth: 0.2,
        borderColor: colors.lightGray,
        borderRadius: radius.lg,
        color: colors.yellow,
        marginLeft: 'auto'
    },
    img: {
        height: 25,
        width: 25,
        resizeMode: 'contain',
        marginRight: margin.md
    },
    btn: {
        flexDirection: 'row',
        alignItems: 'flex-start',
        marginBottom: margin.lg

    },
    text2: {
        fontSize: fonts.mld,
        color: colors.white,
    },
    heading2: {
        fontSize: fonts.xl,
        fontWeight: 'bold',
        color: colors.black,
        marginBottom: margin.sm,
        marginRight: margin.lg
    },
    mapView: {
        flex: 1,
        backgroundColor: colors.white,
        padding: padding.lg,
        borderTopLeftRadius: radius.xl,
        borderTopRightRadius: radius.xl,
        marginTop: -20
    }

}



const mapStateToProp = state => {

    return {
        lang: state.staticLang.userLanguage,
        data: state.instructor_detail.instructor_detail[0],
    }

}

export default connect(mapStateToProp, { setBookedInstructure })(ReviewScreen);