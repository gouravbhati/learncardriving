import React from 'react';
import { Image } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import HomeScreen from './HomeScreen';
import LoginScreen from './LoginScreen';
import SignUpScreen from './SignUpScreen';
import ForgotPasswordScreen2 from './forgot_password_screen2';
import ForgotPasswordScreen from './ForgotPasswordScreen';
import OtpScreen from './OtpScreen';
import AppWelcomeScreen from './AppWelcomeScreen';
import HeaderLogo from '../../utility/HeaderLogo';
import { colors } from '../../../styles/baseStyle';
import Splash from '../../../splash/SplashScreen';


const splash_screen = createStackNavigator({
    AuthLoad: {
        screen: Splash,
        navigationOptions: {
            header: null
        }
    },
});

const AppStackNavigator = createStackNavigator({

    // AuthLoad: {
    //     screen: Splash,
    //     navigationOptions: {
    //         header: null
    //     }
    // },
    login: {
        screen: LoginScreen,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />),
            headerTitle: (() => <HeaderLogo />)

        }
    },

    home: {
        screen: HomeScreen,
        navigationOptions: {
            header: null,
        }
    },
    
    signUp: {
        screen: SignUpScreen,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />),
            headerTitle: (() => <HeaderLogo />)

        }
    },
    forgotPass: {
        screen: ForgotPasswordScreen,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />)

        }
    },
    forgotPass2: {
        screen: ForgotPasswordScreen2,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />)

        }
    },
    otp: {
        screen: OtpScreen,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />)

        }
    },
    appWelcome: {
        screen: AppWelcomeScreen,
        navigationOptions: {
            header: null,
        }
    },

},


    {
        initialRouteName: "home",
        headerBackTitleVisible: false,
        headerLayoutPreset: 'center'

    }

);


const sw = createSwitchNavigator({
    splash:{
        screen: splash_screen
    },
    appStack:{
        screen: AppStackNavigator
    }
},
{
    initialRouteName: 'splash'
}
)
const LoginRoot = createAppContainer(sw)

const Styles = {
    backImg: {
        width: 35,
        height: 20,
        resizeMode: 'contain'
    },
    
}
export default LoginRoot;