initState = {
    sliders: '',
    premium_instructor: '',
    normal_instructor: '',
    booked_instructre: '',
}

export default RootReducer = (state = 0, action) => {
    switch (action.type) {
        case 'SET_SLIDER':
            return { ...state, sliders: action.payload }

        case 'SET_PRIMIUM_INSTRUCTOR':
            return { ...state, premium_instructor: action.payload }

        case 'SET_NORMAL_INSTRUCTOR':
            return { ...state, normal_instructor: action.payload }
        case 'SET_BOOKED_INSTRUCTOR':
            return { ...state, booked_instructre: action.payload }

        default:
            return state;
    }

}
