import React, { Component } from "react";
import { ScrollView } from 'react-native'
import {
    Icon,
    Accordion,
    Text,
    View,
} from "native-base";
import { colors, margin, baseStyles, radius } from "../../../styles/baseStyle";

const dataArray = [
    {
        title: "How it Works?",
        content:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur sunt itaque adipisci quisquam pariatur qui, reiciendis architecto quod sint incidunt labore nisi totam illum numquam non magnam praesentium, maxime quaerat!",
    },
    {
        title: "How can I book a Instructor?",
        content:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur sunt itaque adipisci quisquam pariatur qui, reiciendis architecto quod sint incidunt labore nisi totam illum numquam non magnam praesentium, maxime quaerat!",
    },
    {
        title: "Lorem ipsum dolor sit amet?",
        content:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur sunt itaque adipisci quisquam pariatur qui, reiciendis architecto quod sint incidunt labore nisi totam illum numquam non magnam praesentium, maxime quaerat!",
    },

    {
        title: "How can I book a Instructor?",
        content:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur sunt itaque adipisci quisquam pariatur qui, reiciendis architecto quod sint incidunt labore nisi totam illum numquam non magnam praesentium, maxime quaerat!",
    },
    {
        title: "Lorem ipsum dolor sit amet?",
        content:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur sunt itaque adipisci quisquam pariatur qui, reiciendis architecto quod sint incidunt labore nisi totam illum numquam non magnam praesentium, maxime quaerat!",
    },
    {
        title: "How can I book a Instructor?",
        content:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur sunt itaque adipisci quisquam pariatur qui, reiciendis architecto quod sint incidunt labore nisi totam illum numquam non magnam praesentium, maxime quaerat!",
    },
    {
        title: "Lorem ipsum dolor sit amet?",
        content:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur sunt itaque adipisci quisquam pariatur qui, reiciendis architecto quod sint incidunt labore nisi totam illum numquam non magnam praesentium, maxime quaerat!",
    },
    {
        title: "How can I book a Instructor?",
        content:
            "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur sunt itaque adipisci quisquam pariatur qui, reiciendis architecto quod sint incidunt labore nisi totam illum numquam non magnam praesentium, maxime quaerat!",
    },
];

class FaqScreen extends Component {
    _renderHeader(item, expanded) {
        return (
            <View
                style={{
                    flexDirection: "row",
                    padding: 15,
                    justifyContent: "space-between",
                    alignItems: "center",
                    backgroundColor: colors.offBlack,
                    marginBottom: margin.md,
                    borderRadius: radius.md
                }}
            >
                <Text style={{ fontWeight: "600", color: "#fff" }}>
                    {item.title}
                </Text>
                {expanded
                    ? <Icon style={{ fontSize: 24, color: "#FFE200" }} name="remove" />
                    : <Icon style={{ fontSize: 24, color: "#FFE200" }} name="add" />}
            </View>
        );
    }
    _renderContent(item) {
        return (
            <Text
                style={{
                    backgroundColor: colors.offBlack,
                    padding: 10,
                    marginBottom: 20,
                    color: colors.white,
                    fontStyle: "italic",
                }}
            >
                {item.content}
            </Text>
        );
    }
    render() {
        return (
            <View style={baseStyles.appContainer}>
                <ScrollView>
                    <Accordion
                        style={{ borderWidth: 0 }}
                        dataArray={dataArray}
                        animation={true}
                        expanded={true}
                        renderHeader={this._renderHeader}
                        renderContent={this._renderContent}
                    />
                </ScrollView>
            </View>
        );
    }
}

export default FaqScreen;