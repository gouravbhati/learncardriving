import React, { Component } from 'react';
import { FlatList, ScrollView, View, Text, TouchableOpacity } from 'react-native'
import { baseStyles, colors, fonts, margin } from '../../../styles/baseStyle';
import AppAvatraList from '../../utility/AppAvatarList';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import fetchUserData from '../../utility/api utility/fetchuserdatabyapi';
import { setBookedInstructure } from '../../../store/reducer/actions/rootReducerAction';
import { setOnGoingDetail } from '../../../store/reducer/actions/setOnGoingAction';
import { booked_instructur } from '../../utility/api utility/apis';
import PTRView from 'react-native-pull-to-refresh';

class LearnDashboard extends Component {

    constructor(props) {
        super(props);
        this.demo = [];
        this.slots_by_trainers = {};
        this.state = {
            refresh: '',

        }
    }

    navigateToDetail = (item, toverify) => {
        this.props.setOnGoingDetail(item);
        if (toverify) {
            this.props.navigation.navigate('trainers', { completed: true });
        }
        else {
            this.props.navigation.navigate('trainers');
        }
    }

    isToShow = (id) => {
        let index = this.demo.indexOf(id);
        if (index == -1) {
            this.demo.push(id);
            return true
        }
        else {
            return false
        }
    }

    isSelected = (value) => {
        let num = parseInt(value);
        if (num == 0) {
            return true
        }
        else
            return false
    }

    _refresh = async () => {
        let booked_instructure = await fetchUserData(booked_instructur, await AsyncStorage.getItem('TOKEN'), 'en');
        this.props.setBookedInstructure(booked_instructure.data);

    }


    render() {
        if (this.props.ongoing.length == 0) {
            return <Text style={{ color: 'white' }} >No Program inrolved by you</Text>
        }
        return (

            <View style={[baseStyles.appContainer]}>
                <PTRView onRefresh={this._refresh}>
                    <ScrollView>
                        <View style={baseStyles.marginTopLg}>
                            <Text style={Styles.heading}>{this.props.lang.ongoing}</Text>
                            <FlatList
                                data={this.props.ongoing}
                                numColumns={1}
                                style={{ transform: [{ scaleY: -1 }] }}
                                renderItem={({ item, index }) => {
                                    if (this.isSelected(item.trainee_completed) || this.isSelected(item.instuctor_completed)) {
                                        {
                                            return (
                                                <View style={{ transform: [{ scaleY: -1 }] }}>
                                                    <AppAvatraList
                                                        source={{ uri: null }}
                                                        name={item.instructor_name.name}
                                                        hireing={item.created_at.slice(0, 11)}
                                                        price={"BHD " + item.instructor_name.hourly_rate + "/hr"}
                                                        onPress={() => this.navigateToDetail(item, false)}
                                                    />
                                                </View>
                                            )
                                        }
                                    }
                                    else
                                        null
                                }
                                }
                            />
                        </View>
                        <View style={baseStyles.marginVerticalLg}>
                            <Text style={Styles.heading}>{this.props.lang.completed}</Text>
                            <FlatList
                                data={this.props.ongoing}
                                style={{ transform: [{ scaleY: -1 }] }}
                                numColumns={1}
                                renderItem={({ item, index }) => {
                                    if (!this.isSelected(item.trainee_completed) && !this.isSelected(item.instuctor_completed)) {

                                        return (
                                            <View style={{ transform: [{ scaleY: -1 }] }}>
                                                <AppAvatraList
                                                    source={{ uri: null }}
                                                    name={item.instructor_name.name}
                                                    hireing={item.instructor_name.created_at.slice(0, 11)}
                                                    price={"BHD " + item.instructor_name.hourly_rate + "/hr"}
                                                    onPress={() => this.navigateToDetail(item, true)}
                                                />
                                            </View>
                                        )
                                    }
                                    else
                                        null
                                }
                                }
                            />
                        </View>
                    </ScrollView>
                </PTRView>
            </View>
        );
    }
}

const Styles = {

    heading: {
        color: colors.white,
        fontWeight: 'bold',
        fontSize: fonts.xxl,
        marginBottom: margin.lg
    },

}

const mapstateToProps = (state) => {
    return {
        lang: state.staticLang.userLanguage,
        ongoing: state.rootReducer.booked_instructre
    }
}
export default connect(mapstateToProps, { setBookedInstructure, setOnGoingDetail })(LearnDashboard);