import React from 'react';
import { Image, TouchableOpacity } from 'react-native';
import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer } from 'react-navigation'
import { colors, fonts } from '../../../styles/baseStyle';
import PaymentHistory from '../OtherScreen/PaymentHistory';
import ProfileScreen from '../OtherScreen/ProfileScreen';
import SettingScreen from './setting/SettingScreen';
import NotificationScreen from '../OtherScreen/NotificationScreen';
import AccountInfo from './setting/AccountInfo';
import ChangePassword from './setting/ChangePassword';
import FaqScreen from '../OtherScreen/FaqScreen';
import HeaderTitle from '../../../static language/staticlangHeader';
import LanguageSetting from '../OtherScreen/setting/language_setting';
const ProfileHeader = (navigation) => {
    return (
        <TouchableOpacity onPress={() => navigation('dashboard')}>
            <Image source={require('../../../assets/back.png')} style={{ width: 35, height: 20, resizeMode: 'contain', marginLeft: 15 }} />
        </TouchableOpacity>
    )
}

const ProfileNavigation = createStackNavigator({

    profile: {
        screen: ProfileScreen,
        navigationOptions: ({ navigation }) => ({
            headerStyle: {
                backgroundColor: 'black'
            },
            headerTitle: <HeaderTitle title={'Profile'} />,
            headerTitleStyle: {
                color: colors.white,
                fontSize: fonts.xl,
                fontWeight: 'bold',
                //paddingLeft: 100,
                alignSelf: 'center'
            },

            headerLeft: ProfileHeader(navigation.navigate)
        })
    },
    payment: {
        screen: PaymentHistory,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />),
            headerTitle: <HeaderTitle title={'Payment History'} />,
            headerTitleStyle: { color: colors.white, fontSize: fonts.xl, fontWeight: 'bold' },
        }
    },
    setting: {
        screen: SettingScreen,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />),
            headerTitle: <HeaderTitle title={'Explore'} />,
            headerTitleStyle: { color: colors.white, fontSize: fonts.xl, fontWeight: 'bold', textAlign : 'center' },
        }
    },
    notification: {
        screen: NotificationScreen,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />),
            headerTitle: <HeaderTitle title={'Notification'} />,
            headerTitleStyle: { color: colors.white, fontSize: fonts.xl, fontWeight: 'bold' },
        }
    },
    account: {
        screen: AccountInfo,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />),
            headerTitle: <HeaderTitle title={'Account Information'} />,
            headerTitleStyle: { color: colors.white, fontSize: fonts.xl, fontWeight: 'bold' },
        }
    },
    changePass: {
        screen: ChangePassword,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />),
            headerTitle: <HeaderTitle title={"Change Password"} />,
            headerTitleStyle: { color: colors.white, fontSize: fonts.xl, fontWeight: 'bold' },
        }
    },
    language_setting: {
        screen: LanguageSetting,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />),
            headerTitle: <HeaderTitle title={"Language Setting"} />,
            headerTitleStyle: { color: colors.white, fontSize: fonts.xl, fontWeight: 'bold',  },
        }
    },
    faq: {
        screen: FaqScreen,
        navigationOptions: {
            headerStyle: { backgroundColor: colors.black, borderBottomColor: null },
            headerBackImage: (() => <Image source={require('../../../assets/back.png')} style={Styles.backImg} />),
            headerTitle: <HeaderTitle title={'Explore'} />,
            headerTitleStyle: { color: colors.white, fontSize: fonts.xl, fontWeight: 'bold',textAlign: 'center', },
        }
    },
},
    {
        initialRouteName: "profile",
        headerBackTitleVisible: false,
        headerLayoutPreset : 'center'
    }
)

const ProfileRoot = createAppContainer(ProfileNavigation)

const Styles = {
    backImg: {
        width: 35,
        height: 20,
        resizeMode: 'contain'
    }
}
export default ProfileRoot;