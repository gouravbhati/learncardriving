import { Arabic, English } from '../../../static language/lanuage';


export const setStaticLangArabic = () => {
    return {
        type: 'SET_USER_LANGUAGE',
        payload: Arabic
    }
}

export const setStaticLangEnglish = () => {
    return {
        type: 'SET_USER_LANGUAGE',
        payload: English
    }
}