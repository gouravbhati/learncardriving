import React, { Component } from 'react';
import { View, FlatList, Text, ScrollView, TouchableOpacity, Modal, ActivityIndicator, Image } from 'react-native'
import { baseStyles, padding, margin, fonts, colors, radius } from '../../../styles/baseStyle';
import AsyncStorage from '@react-native-community/async-storage';
import { DatePicker, Input, Item, Icon } from 'native-base';
import AppButton from '../../utility/AppButton';
import fetchSlote from '../../utility/api utility/fetchuserdatabyapi';
import Snackbar from 'react-native-snackbar';
import { connect } from 'react-redux';
import Activity from '../../utility/activity_indicator';

class BookInstructor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            radioBtnsData: ['Yes', 'No'],
            checked: 0,
            token: '',
            slotes: '',
            date: '',
            isDateSelect: false,
            timeslot: '',
            selectedslot: null,
            totalclass: '',
            time: '',
            id: '',
            class_placeholder: this.props.lang.classes,
        }
    }

    componentDidMount = async () => {
        let token = await AsyncStorage.getItem('TOKEN');
        this.setState({ token });
        this.setState({ id: this.props.navigation.getParam('id') })
    }

    fetchSlotByDate = async (date, id) => {
        let result = await fetchSlote(`http://168.235.81.165/cardrive/public/api/user/instructor-all-slot/${date}/${id}`, this.state.token, 'en');
        if (result.data.length == 0) {
            this.setState({ isDateSelect: false });
            this.setState({ slotes: null });
        }
        else {
            this.setState({ slotes: result.data });
            this.setState({ isDateSelect: false });
        }
    }

    getLocationPermmision = async () => {
        try {
            let respomse = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
        } catch (err) {
            console.warn(err);
        }
    }

    updateDate = async (rawdate) => {
        let response = rawdate.toString();
        let date = response.split(" ");
        let day = date[2];
        let year = date[3];
        let mont = this.getMonth(date[1]);
        this.setState({ date: `${year}-${mont}-${day}` });
        this.setState({ isDateSelect: true });
    }

    getMonth = (mon) => {
        switch (mon) {
            case 'Jan':
                return '01'
            case 'Feb':
                return '02'
            case 'Mar':
                return '03'
            case 'Apr':
                return '04'
            case 'May':
                return '05'
            case 'Jun':
                return '06'
            case 'Jul':
                return '07'
            case 'Aug':
                return '08'
            case 'Sep':
                return '09'
            case 'Oct':
                return '10'
            case 'Nov':
                return '11'
            case 'Dec':
                return '12'
        }
    }

    renderActivity = () => {
        if (this.state.isDateSelect)
            return (
                <Modal transparent={true} >
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                        <ActivityIndicator size='large' color='red' />
                    </View>
                </Modal>
            )
        return null
    }


    navigateToPay = () => {
        if (this.state.slotes && this.state.selectedslot && this.state.totalclass != 0) {
            this.props.navigation.navigate('review', { slot_id: this.state.selectedslot, selectedclasses: this.state.totalclass, time: this.state.time });
        }
        else {
            Snackbar.show({
                text: 'Select a date and slot',
                duration: 4000
            })
        }
    }


    diff = (start, end) => {
        start = start.split(":");
        end = end.split(":");
        var startDate = new Date(0, 0, 0, start[0], start[1], 0);
        var endDate = new Date(0, 0, 0, end[0], end[1], 0);
        var diff = endDate.getTime() - startDate.getTime();
        var hours = Math.floor(diff / 1000 / 60 / 60);
        diff -= hours * 1000 * 60 * 60;
        var minutes = Math.floor(diff / 1000 / 60);
        if (hours < 0)
            hours = hours + 24;

        return (hours <= 9 ? "0" : "") + hours + ":" + (minutes <= 9 ? "0" : "") + minutes;
    }



    renderSlotes = () => {
        if (this.state.slotes) {
            return (
                <FlatList
                    data={this.state.slotes}
                    renderItem={({ item }) =>
                        <View style={{
                            justifyContent: 'flex-start', alignItems: 'center', flex: 1, flexDirection: 'row', margin: margin.md, padding: padding.md, borderRadius: radius.md,
                            backgroundColor: this.state.selectedslot == item.id ? '#FFE200' : '#212121'
                        }}>
                            <Icon name="time" style={{ color: this.state.selectedslot == item.id ? '#000' : '#fff', fontSize: 18, marginRight: margin.md }} />
                            <TouchableOpacity onPress={() => this.setState({ selectedslot: item.id, time: this.diff(item.available_start_time, item.available_end_time) })}>
                                <Text style={{ color: this.state.selectedslot == item.id ? '#000' : '#fff', fontSize: 16 }} >{this.props.lang.time}{` : ${item.available_start_time}${item.start_time_meridiem} - ${item.available_end_time}${item.end_time_meridiem} `}</Text>
                            </TouchableOpacity>
                        </View>
                    }
                />
            )
        }
        return null
    }


    updateClass = (value) => {
        if (value > 25) {
            Snackbar.show({
                text: "You can't select class more than 25",
                duration: 4000
            });
            let reset = parseInt(value / 10)
            return reset
        }
        else {
            return value
        }
    }


    render() {
        this.state.isDateSelect ? this.fetchSlotByDate(this.state.date, this.state.id) : null

        return (
            <View style={[baseStyles.appContainer, Styles.container]}>
                <ScrollView>
                    <Activity show={this.state.isDateSelect} />
                    <View style={baseStyles.paddingLg}>
                        <View style={Styles.rowContainer}>
                            <Text style={Styles.heading}>{this.props.lang.classes}</Text>
                            <Item regular style={{ width: '43%', marginLeft: 'auto', borderRadius: radius.md, height: 40 }}>
                                <Input
                                    value={this.state.totalclass}
                                    maxLength={2}
                                    style={{ color: '#fff' }}
                                    placeholder={this.state.class_placeholder}
                                    placeholderTextColor="white"
                                    onFocus={() => { this.setState({ class_placeholder: '' }) }}
                                    onEndEditing={() => { this.setState({ class_placeholder: this.props.lang.classes }) }}
                                    onChangeText={(value) => this.setState({ totalclass: this.updateClass(value.replace(/[^0-9]/g, '')) })}
                                    keyboardType='number-pad'
                                />
                            </Item>
                        </View>
                        <View style={[baseStyles.flexRow, baseStyles.justifyContentBetween, baseStyles.alignItemsCenter, baseStyles.marginTopMd]}>
                            <Text style={Styles.heading}>{this.props.lang.start_date}</Text>
                            <View style={{ borderWidth: 1, borderColor: colors.gray, borderRadius: radius.md, flexDirection: 'row', alignItems: "center", paddingHorizontal: padding.md }}>
                                <Image source={require('../../../assets/calendar.png')} style={{ height: 20, width: 20, resizeMode: 'contain' }} />
                                <DatePicker
                                    locale={"en"}
                                    minimumDate={new Date()}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={true}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    placeHolderText={this.props.lang.start_date}
                                    textStyle={{ color: "white", fontSize: 16 }}
                                    placeHolderTextStyle={{ color: "#d3d3d3" }}
                                    onDateChange={(date) => this.updateDate(date)}
                                />
                            </View>
                        </View>

                        <View style={[baseStyles.flexRow, baseStyles.justifyContentBetween, baseStyles.alignItemsCenter, baseStyles.marginTopMd]}>
                            <Text style={Styles.heading}>{this.props.lang.time}</Text>
                        </View>
                        {this.renderSlotes()}
                    </View>

                    <View style={{ paddingHorizontal: padding.lg, marginTop: margin.lg }}>

                        <View style={{ justifyContent: 'flex-end' }}>
                            <AppButton
                                title={this.props.lang.schedule}
                                btnStyle={{
                                    borderRadius: radius.xxl,
                                    borderTopLeftRadius: 0,
                                    paddingHorizontal: padding.xxl
                                }}
                                onPress={() => this.navigateToPay()}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const Styles = {
    rowContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: margin.sm,
    },
    checkBoxStyle: {
        height: 40,
        width: 40
    },
    container: {
        paddingHorizontal: 0
    },
    heading: {
        fontSize: fonts.xl,
        color: colors.white,
    },
    img: {
        height: 25,
        width: 25,
        resizeMode: 'contain',
        marginRight: margin.md
    },
    btn: {
        flexDirection: 'row',
        alignItems: 'center'

    },
    text: {
        fontSize: fonts.xl,
        color: colors.white,
    }
}

const mapStateToProp = state => {

    return {
        lang: state.staticLang.userLanguage,
    }

}

export default connect(mapStateToProp)(BookInstructor);

