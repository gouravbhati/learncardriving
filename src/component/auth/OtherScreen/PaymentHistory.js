import React, { Component } from 'react';
import { FlatList, ScrollView, Image } from 'react-native'
import { Card, View, Text } from 'native-base'
import { baseStyles, colors, padding, radius, fonts, margin } from '../../../styles/baseStyle';
import AppButton from '../../utility/AppButton';

export default class PaymentHistory extends Component {

    constructor(props) {
        super(props);
        this.state = {
            payList: [
                {
                    date: "27/09/2019",
                    paypal: require('../../../assets/paypal.png'),
                    process: "processing",
                    currency: "BHD 99.999"
                },
                {
                    date: "2/09/2019",
                    paypal: require('../../../assets/paypal.png'),
                    process: "sucessfull",
                    currency: "BHD 99.999"
                },
                {
                    date: "2/09/2019",
                    paypal: require('../../../assets/paypal.png'),
                    process: "sucessfull",
                    currency: "BHD 99.999"
                },

            ]
        }
    }
    render() {
        return (
            <View style={baseStyles.appContainer}>
                <ScrollView>
                    <FlatList
                        data={this.state.payList}
                        numColumns={1}
                        renderItem={({ item }) =>
                            <Card style={Styles.cardStyle}>
                                <View style={[Styles.row, baseStyles.marginBottomLg]}>
                                    <Text style={Styles.date}>{item.date}</Text>
                                    <Text style={Styles.heading}>{item.currency}</Text>
                                </View>

                                <View style={Styles.row}>
                                    <Image source={item.paypal} style={Styles.imgStyle} />
                                    <AppButton
                                        title={item.process}
                                        titleStyle={{ fontSize: fonts.md }}
                                        btnStyle={{ marginVertical: 0, height: 30 }} />
                                </View>
                            </Card>
                        }
                    />
                </ScrollView>
            </View>
        );
    }
}

const Styles = {
    cardStyle: {
        backgroundColor: colors.offBlack,
        padding: padding.lg,
        borderColor: 0,
        borderRadius: radius.lg
    },
    heading: {
        color: colors.white,
        fontWeight: 'bold',
        fontSize: fonts.xl,
    },
    date: {
        color: colors.white,
        fontSize: fonts.lg,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',

    },
    imgStyle: {
        width: 80,
        height: 20,
        resizeMode: 'contain'
    }
}