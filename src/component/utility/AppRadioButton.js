import React, { Component } from 'react';
import { View, Image, TouchableOpacity, Text } from 'react-native';
import { colors, radius, margin, padding, fonts, baseStyles } from '../../styles/baseStyle';


export default class AppRadioButton extends Component {
    constructor(props) {
        super(props);
        this.state = {
            radioBtnsData: ['Will Go to Trainer\'s Location \n Rd No 2151, Manama, Bahrain', 
            'Trainer will come to my Address'],
            checked: 0
        }
    }
    render() {
        return (
            <View>
                {this.state.radioBtnsData.map((data, key) => {
                    return (
                        <View key={key} style={[baseStyles.marginBottomMd,baseStyles.paddingTopLg]}>
                            {this.state.checked == key ?
                                <TouchableOpacity style={styles.btn}>
                                    <Image style={styles.img} source={require("../../assets/radio.png")} />
                                    <Text style={styles.text}>{data}</Text>
                                </TouchableOpacity>
                                :
                                <TouchableOpacity onPress={() => { this.setState({ checked: key }) }} style={styles.btn}>
                                    <Image style={styles.img} source={require("../../assets/radio_circle.png")} />
                                    <Text style={styles.text}>{data}</Text>
                                </TouchableOpacity>
                            }
                        </View>
                    )
                })}
            </View>
        );
    }
}

const styles = {
    img: {
        height: 25,
        width: 25,
        resizeMode: 'contain',
        marginRight: margin.md
    },
    btn: {
        flexDirection: 'row',
        alignItems: 'center'

    },
    text:{
        fontSize:fonts.xl,
        color:colors.white,
    }
}