import React, { Component } from 'react';
import { TouchableOpacity, Image } from 'react-native';
import { Thumbnail, View } from 'native-base';

class AppAvatar extends Component {

    renderImage = () => {
        if (this.props.navigatetomap)
            return (
                <TouchableOpacity onPress={() => this.props.navigation.navigate('mapview')}>
                    <Image
                        style={{ marginRight: 10, marginTop: 5 }}
                        source={require('../../assets/map.png')}
                    />
                </TouchableOpacity>
            );
        return (
            <TouchableOpacity onPress={() => this.props.navigation.navigate('dashboard')}>
                <Image
                    style={{ marginRight: 10, marginTop: 5 }}
                    source={require('../../assets/map.png')}
                />
            </TouchableOpacity>
        )
    }

    render() {
        const { navigation } = this.props
        return (
            <View style={{ flexDirection: 'row' }} >
                <this.renderImage />
                <TouchableOpacity onPress={() => navigation.navigate('profile')} >
                    <Thumbnail small
                        source={require('../../assets/user.png')}
                    />
                </TouchableOpacity>
            </View>
        );
    }

}
export default AppAvatar;;