import React, { Component } from 'react';
import { ScrollView, FlatList, Modal, Alert, Image } from 'react-native'
import { Card, View, Text, } from 'native-base'
import { baseStyles, colors, padding, radius, fonts, margin } from '../../../styles/baseStyle';
import AsyncStorage from '@react-native-community/async-storage';
import AppAvatraList from '../../utility/AppAvatarList';
import AppButton from '../../utility/AppButton';
import AppCheckBox from '../../utility/AppCheckBox';
import AppIconInput from '../../utility/AppIconInput';
import Activity from '../../utility/activity_indicator';
import AppSlider from '../../utility/AppSlider';
import { connect } from 'react-redux';

class TrainingDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            checked: true,
            token: '',
            cancleblock: '',
            instrcutre_completed: '',
            activity: false
        }
    }

    componentDidMount = () => {
        let trainee_completed = parseInt(this.props.instructureiongoingdetail.trainee_completed);
        let instuctor_completed = parseInt(this.props.instructureiongoingdetail.instuctor_completed);
        console.log(this.props.instructureiongoingdetail.id);
        let verify = this.props.navigation.getParam('completed');
        this.setState({ cancleblock: verify });
        if (trainee_completed == 0) {
            this.setState({ checked: false });
        }
        else
            this.setState({ checked: true });
    }



    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    getTime = (item) => {
        return (`${item.available_start_time}${item.start_time_meridiem} - ${item.available_end_time}${item.end_time_meridiem} `)
    }

    getDay = (item) => {
        let dat = new Date(item.available_date);
        switch (dat.getDay()) {
            case 0:
                return 'Sunday'
            case 1:
                return 'Monday'
            case 2:
                return 'Tuesday'
            case 3:
                return 'Wednesday'
            case 4:
                return 'Thursday'
            case 5:
                return 'Friday'
            case 6:
                return 'Saturday'
        }
    }

    showPopUp = () => {
        Alert.alert(
            'Slots',
            'Are you sure to change the schedule',
            [{
                text: 'Cancel',
                onPress: () => { },
                style: 'cancel',
            },
            { text: 'OK', onPress: () => this.updateActionchange() },
            ],
            { cancelable: false },
        );
    }


    actionforChangeState = async () => {
        this.setState({ activity: true });
        let res = await fetch("http://168.235.81.165/cardrive/public/api/user/user-completed-reject", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${await AsyncStorage.getItem('TOKEN')}`,
            },
            body: JSON.stringify({
                "trainee_completed": !this.state.checked ? 1 : 0,
                "booking_id": this.props.instructureiongoingdetail.id
            })
        });
        let result = await res.json();
        this.setState({ activity: false });
        return result
    }

    updateActionchange = async () => {
        this.setState({ checked: !this.state.checked });
        let response = await this.actionforChangeState();
        console.log(response);
    }

    showCancleBolck = () => {
        // if (!this.state.cancleblock) {
        //     return (
        //         <>
        //             <AppButton
        //                 title="Ongoing"
        //                 btnStyle={Styles.statusBtn} />
        //             <AppButton
        //                 title="Cancel"
        //                 titleStyle={{ color: colors.white }}
        //                 btnStyle={Styles.cancleBtn}
        //                 onPress={() => { this.setModalVisible(true); }}
        //             />
        //         </>
        //     )
        // }
        // else {
        return null
        // }
    }

    appchack = () => {
        if (!this.state.cancleblock) {
            return (
                <>
                    <AppCheckBox
                        checkBoxStyle={Styles.checkBoxStyle}
                        value={this.state.checked}
                        onPress={() => this.showPopUp()}
                    />
                </>
            )
        }
        else {
            return (
                <Image
                    source={require('../../../assets/checklist.png')}
                    style={{ width: 40, height: 40 }}
                />

            )
        }

    }

    verifybytrainer = () => {

        if (this.state.instrcutre_completed)
            return (
                <Text style={{ color: 'white', textAlign: 'right' }}> Trainer Verifyed </Text>
            )
        else
            return null
    }



    render() {
        return (
            <View style={[baseStyles.appContainer, Styles.container]}>
                <Activity show={this.state.activity} />
                <ScrollView>
                    <AppSlider
                        disabled={false}
                        value={12}
                    />
                    <View style={{ backgroundColor: colors.offBlack, padding: padding.lg }}>
                        <AppAvatraList
                            source={{ uri: this.props.instructureiongoingdetail.instructor_name.profile_url }}
                            name={this.props.instructureiongoingdetail.instructor_name.name}
                            hireing={this.props.instructureiongoingdetail.instructor_name.created_at.slice(0, 11)}
                            price={"BHD " + this.props.instructureiongoingdetail.instructor_name.hourly_rate + "/hr"}
                        />
                        <View style={{ justifyContent: 'flex-end', flexDirection: 'row' }}>
                            <this.showCancleBolck />
                        </View>
                    </View>

                    <View style={baseStyles.paddingLg}>
                        <Text style={Styles.heading}> {this.props.lang.class_detail} </Text>
                    </View>

                    <View style={{ paddingHorizontal: padding.md }}>
                        <FlatList
                            data={this.props.instructureiongoingdetail.slots}
                            numColumns={1}
                            renderItem={({ item }) =>
                                <View>
                                    <Text style={[baseStyles.whiteText, baseStyles.marginBottomMd]}>{this.getDay(item)}</Text>
                                    <this.verifybytrainer />
                                    <View style={Styles.rowContainer}>
                                        <AppIconInput editable={false} iconName="calendar" placeholder={item.available_date} />
                                        <AppIconInput editable={false} iconName="time" placeholder={this.getTime(item)} />
                                        <this.appchack />
                                    </View>
                                </View>
                            } />
                    </View>

                    <Card style={Styles.cardStyle}>
                        <View style={[Styles.row, baseStyles.marginBottomLg]}>
                            <Text style={Styles.text}>{this.props.lang.total_hour_train}</Text>
                            <Text style={Styles.text}>{this.props.lang.tot_amu}</Text>
                        </View>
                        <View style={Styles.row}>
                            <Text style={Styles.heading}>4 Hours </Text>
                            <Text style={Styles.heading}>BHD {this.props.instructureiongoingdetail.amount}</Text>
                        </View>
                    </Card>
                </ScrollView>


                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', margin: 30 }}>
                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.modalVisible}
                    >
                        <View style={Styles.modalDialog}>
                            <View>
                                <Text style={{ color: colors.white, textAlign: 'center', fontSize: fonts.lg }}>Are you sure you want to Cancel this training</Text>
                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                                    <AppButton
                                        onPress={() => {
                                            this.setModalVisible(!this.state.modalVisible);
                                        }}
                                        title="No"
                                        titleStyle={{ color: colors.white }}
                                        btnStyle={{ marginBottom: 0, backgroundColor: colors.red, width: 90 }}
                                    />
                                    <AppButton
                                        title="Yes"
                                        btnStyle={{ marginBottom: 0, marginLeft: margin.md, width: 90 }}
                                        onPress={() => this.props.navigation.navigate('sucessTranee')} />
                                </View>

                            </View>
                        </View>
                    </Modal>
                </View>
            </View>
        );
    }
}

const Styles = {

    heading: {
        color: colors.white,
        fontWeight: 'bold',
        fontSize: 30,
        marginBottom: margin.lg
    },
    container: {
        paddingHorizontal: 0
    },
    rowContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: margin.lg
    },
    checkBoxStyle: {
        height: 30,
        width: 30
    },
    cardStyle: {
        backgroundColor: colors.offBlack,
        padding: padding.lg,
        borderColor: 0,
        borderRadius: radius.lg
    },
    heading: {
        color: colors.white,
        fontWeight: 'bold',
        fontSize: fonts.xl,
    },
    text: {
        color: colors.white,
        fontSize: fonts.lg,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',

    },
    cancleBtn: {
        marginBottom: 0,
        marginTop: margin.md,
        backgroundColor: colors.red,
        marginLeft: margin.lg
    },
    statusBtn: {
        marginBottom: 0,
        marginTop: margin.md
    },
    modalDialog: {
        position: 'absolute',
        backgroundColor: colors.offBlack,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        margin: 'auto',
        top: '30%',
        left: 0,
        right: 0,
        width: null,
        height: null,
        borderRadius: radius.lg,
        padding: padding.lg,
        borderWidth: 0.4,
        borderColor: colors.yellow,
        margin: margin.xl
    }
}

const mapStateToProp = state => {

    return {
        lang: state.staticLang.userLanguage,
        instructureiongoingdetail: state.ongoing.onGoingDetail

    }

}

export default connect(mapStateToProp)(TrainingDetail);