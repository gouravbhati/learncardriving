import React, { Component } from 'react';
import { View, Text, FlatList, ScrollView } from 'react-native';
import { connect } from 'react-redux';
import { baseStyles, radius, fonts, margin, colors, padding } from '../../../styles/baseStyle';
import { Thumbnail } from 'native-base';
import { instructor } from './data'
import AppButton from '../../utility/AppButton';
import rating from '../../utility/StarRating';
class InstructorDetail extends Component {
    state = {
        date: '',
        rating: ''
    }

    componentDidMount() {
        this.setState({ rating: this.props.navigation.getParam('rating') });
    }

    render() {
        return (
            <View style={[baseStyles.appContainer, Styles.container]}>
                <ScrollView>
                    <View style={Styles.profileCard}>
                        <Thumbnail
                            square
                            source={{ uri: this.props.data.profile_url }}
                            style={Styles.profileImg}
                        />
                        <Text style={Styles.name}>{this.props.data.name}</Text>
                        <View style={{ flexDirection: 'row' }} >
                            {rating(parseInt(this.state.rating)).map(value => value)}
                        </View>
                    </View>

                    <View style={[baseStyles.flexRow, baseStyles.justifyContentBetween, baseStyles.marginVerticalLg]}>
                        <View style={Styles.column}>
                            <Text style={Styles.text}>{this.props.lang.member_since}</Text>
                            <Text style={Styles.textBold}>{this.props.date}</Text>
                        </View>
                        <View style={Styles.column}>
                            <Text style={Styles.text}>{this.props.lang.experience}</Text>
                            <Text style={Styles.textBold}>{this.props.data.year_of_experience} Years</Text>
                        </View>
                        <View style={Styles.column}>
                            <Text style={Styles.text}>{this.props.lang.total_dri_hour}</Text>
                            <Text style={Styles.textBold}>{this.props.data.hourly_rate}</Text>
                        </View>
                        <View style={Styles.column}>
                            <Text style={Styles.text}>{this.props.lang.ongoing}</Text>
                            <Text style={Styles.textBold}>{instructor.ongoing}</Text>
                        </View>
                    </View>

                    <View style={Styles.card}>
                        <Text style={Styles.heading}>{this.props.lang.about_instructor}</Text>
                        <View style={baseStyles.flexRow}>
                            <Thumbnail square source={require('../../../assets/location.png')} style={Styles.iconStyle} />
                            <Text style={Styles.text}> {this.props.data.city} , {this.props.data.state}</Text>
                        </View>
                        <Text style={baseStyles.whiteText}>{this.props.data.instructor_Detail}</Text>
                    </View>

                    <View style={Styles.card}>
                        <Text style={Styles.heading}>{this.props.lang.rating_and_review}</Text>
                        <FlatList
                            data={this.props.data.rating_with_users}
                            renderItem={({ item }) =>
                                <View style={[baseStyles.media, Styles.border]}>
                                    <Thumbnail source={{ uri: null }} style={Styles.profile} />
                                    <View style={baseStyles.mediaBody}>
                                    <Text style={{ color: 'white' }} >Aslam</Text>
                                        <View style={{ flexDirection: 'row', marginBottom: margin.sm }}>
                                            {rating(parseInt(item.rating)).map(value => value)}
                                        </View>
                                        <Text style={baseStyles.whiteText}>{item.review}</Text>
                                    </View>
                                </View>
                            }
                        />
                    </View>

                    <View style={{ justifyContent: 'flex-end' }}>
                        <AppButton
                            title={this.props.lang.book_instructor}
                            btnStyle={{
                                borderRadius: radius.xxl,
                                borderTopLeftRadius: 0,
                                paddingHorizontal: padding.xxl
                            }}
                            onPress={() => this.props.navigation.navigate('bookInstructor', { id: this.props.data.id })}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const Styles = {

    container: {
        justifyContent: 'flex-start'
    },
    profileCard: {
        alignItems: 'center'
    },
    profileImg: {
        height: 150,
        width: 150,
        borderRadius: radius.lg
    },
    rating: {
        width: 100,
        height: 20,
        resizeMode: 'contain'
    },
    name: {
        fontSize: fonts.xl,
        fontWeight: 'bold',
        marginVertical: margin.md,
        color: colors.white
    },
    column: {
        width: '48%',
        alignSelf: 'center',
        backgroundColor: colors.offBlack,
        padding: padding.lg,
        marginBottom: margin.md,
    },
    text: {
        color: colors.white,
        fontSize: fonts.md,
        marginBottom: margin.md
    },
    textBold: {
        color: colors.white,
        fontSize: fonts.lg,
        fontWeight: 'bold'
    },
    card: {
        borderRadius: radius.lg,
        backgroundColor: colors.offBlack,
        padding: padding.lg,
        marginBottom: margin.lg
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    heading: {
        fontSize: fonts.xl,
        color: colors.white,
        fontWeight: 'bold',
        marginBottom: margin.md
    },
    iconStyle: {
        height: 20,
        width: 20,
        resizeMode: 'contain'
    },
    mlAuto: {
        marginLeft: 'auto'
    },
    profile: {
        height: 70,
        width: 70,
        borderRadius: 70 / 2,
        marginRight: margin.md
    },
    border: {
        borderBottomWidth: 0.5,
        borderBottomColor: '#9B9B9B',
        paddingBottom: padding.lg,
        marginBottom: margin.lg,
        alignItems: 'flex-start'
    }

}

const mapStateToProp = state => {

    return {
        lang: state.staticLang.userLanguage,
        data: state.instructor_detail.instructor_detail[0],
        date: state.instructor_detail.instructor_detail[0].created_at.slice(0, 10),
    }

}

export default connect(mapStateToProp)(InstructorDetail);