import React from 'react';
import { Icon } from 'native-base';

function _renderStarRating(torate) {
    let stars = [];
    for (var i = 1; i <= 5; i++) {
        if (i <= torate)
            stars.push((<Icon name="star" style={{ color: 'yellow' }} />));
        else
            stars.push((<Icon name="star" style={{ color: 'white' }} />));
    }
    return stars
}
export default _renderStarRating