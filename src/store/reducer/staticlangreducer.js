initialState = {
    userLanguage: ''
}

export default StaticLanguageReducer = (status = initialState, action) => {
    switch (action.type) {
        case 'SET_USER_LANGUAGE':
            return { ...status, userLanguage: action.payload }
        default:
            return status
    }
}