import React, { Component } from 'react';
import { View, ActivityIndicator } from 'react-native'
import { colors, margin, fonts, baseStyles, padding } from '../../../../styles/baseStyle';
import AppInput from '../../../utility/AppInput';
import AppButton from '../../../utility/AppButton';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-community/async-storage';
import { connect } from 'react-redux';
import Activity from '../../../utility/activity_indicator';

class ChangePassword extends Component {

    state = {
        curr_pass: '',
        curr_pass_er: '',

        new_pass: '',
        new_pass_er: '',

        re_pass: '',
        re_pass_er: '',

        email: '',
        activity: false,

        password: ''


    }
    async componentDidMount() {
        let email_ = await AsyncStorage.getItem('email');
        let password_ = await AsyncStorage.getItem('password');
        this.setState({ email: email_, password: password_ });
    }
    showSnackBar = (message) => {
        Snackbar.show({
            text: message,
            duration: 2000
        })
    }
    Validate() {
        if (this.state.curr_pass === '') {
            this.showSnackBar('Current Password is Required');
            return 0
        }
        if (this.state.new_pass === '') {
            this.showSnackBar('New Password is Required');
            return 0
        }
        if (this.state.new_pass !== this.state.re_pass) {
            this.showSnackBar('New and Re-Enter Password should be Same');
            return 0
        }
        if (this.state.re_pass === '') {
            this.showSnackBar('Re-Enter Password is Required');
            return 0
        }
        else {
            this.reset_password_Api();
        }
    }
    reset_password_Api = async () => {
        if (this.state.password === this.state.curr_pass) {
            this.setState({ activity: true })
            let res = await fetch('http://168.235.81.165/cardrive/public/api/user/reset-password', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: this.state.email,
                    password: this.state.new_pass,
                })
            });
            let result = await res.json();
            console.log(result);
            if (result.success) {

                setTimeout(() => {
                    this.setState({ activity: false })
                    this.showSnackBar('Password Changed Successfully');
                    AsyncStorage.setItem('password', this.state.new_pass);
                    this.props.navigation.goBack();
                }, 5000);
            }
            else {
                this.setState({ activity: true })
                setTimeout(() => {
                    this.setState({ activity: false })
                    this.showSnackBar('Request Failed');

                }, 5000);
            }
        }
        else {
            this.setState({ activity: false })
            this.showSnackBar('Current Password is Wrong!');
        }
    }

    activity_indicator() {
        if (this.state.activity === true) {
            return <ActivityIndicator size="large" color="#ffe200" />
        }
        else {
            return null;
        }
    }
    render() {
        return (
            <View style={[baseStyles.appContainer, Styles.container]}>
                <View style={Styles.innerContainer}>
                    <AppInput label={this.props.lang.current_pass}
                        onChangeText={(text) => this.setState({ curr_pass: text })}
                    />
                    <AppInput label={this.props.lang.enter_pass}
                        onChangeText={(text) => this.setState({ new_pass: text })}
                    />
                    <AppInput label={this.props.lang.reenter_pass}
                        onChangeText={(text) => this.setState({ re_pass: text })}
                    />
                </View>
                <Activity show={this.state.activity} />
                <View style={{ justifyContent: 'flex-end', flex: 1, }}>
                    <AppButton title={this.props.lang.save_pass}
                        onPress={() => {
                            //this.reset_password_Api();
                            this.Validate();
                        }}
                    />
                </View>

            </View>

        );
    }
}

const Styles = {

    container: {
        paddingHorizontal: 0,
        justifyContent: 'flex-start',
    },
    listItemStyle: {
        backgroundColor: colors.offBlack,
        marginBottom: margin.sm,
        height: 60,
        paddingLeft: 30
    },
    innerContainer:
    {
        backgroundColor: colors.offBlack,
        paddingHorizontal: padding.lg,
        paddingVertical: padding.xl

    },
    listText: {
        color: colors.white,
        fontSize: fonts.xl
    },
}

const mapstateToProps = (state) => {
    return {
        lang: state.staticLang.userLanguage
    }
}
export default connect(mapstateToProps)(ChangePassword);