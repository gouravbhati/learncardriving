import React, { Component } from "react";
import { Container, Header, Button, Content, ActionSheet, Text } from "native-base";
const BUTTONS = ["Option 0", "Option 1", "Option 2", "Delete", "Cancel"];
const DESTRUCTIVE_INDEX = 0;
const CANCEL_INDEX = 4;
export default class AppActionSheet extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
     
          <Button
            onPress={() =>
            ActionSheet.show(
              {
                options: BUTTONS,
                cancelButtonIndex: CANCEL_INDEX,
                destructiveButtonIndex: DESTRUCTIVE_INDEX,
              },
              buttonIndex => {
                this.setState({ clicked: BUTTONS[buttonIndex] });
              }
            )}
          >
            <Text>Actionsheet</Text>
          </Button>
       
    );
  }
}