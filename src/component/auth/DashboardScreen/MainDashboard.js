import React, { Component } from 'react';
import ImageSlider from 'react-native-image-slider';
import { connect } from 'react-redux';
import { Image, FlatList, ScrollView, TouchableOpacity } from 'react-native';
import { View, Item, Icon, Input, Text, Thumbnail } from 'native-base';
import Snackbar from 'react-native-snackbar';
import { baseStyles, colors, padding, radius, margin, fonts } from '../../../styles/baseStyle';
import rating from '../../utility/StarRating';
import { Instructor_detail } from '../../../store/reducer/actions/instructor_detail';
import AsyncStorage from '@react-native-community/async-storage';
import Activity from '../../utility/activity_indicator';

class MainDashboard extends Component {
    state = {
        token: '',
        activity: false
    }

    async componentDidMount() {
        let token1 = await AsyncStorage.getItem('TOKEN');
        this.setState({ token: token1 });
        this.sliderimg = this.props.slider.map((item) => {
            return item.image_url
        });
    }

    callApi = async (item) => {
        this.setState({ activity: true });
        try {
            let res = await fetch("http://168.235.81.165/cardrive/public/api/user/instructor-details/" + item.id, {
                method: 'GET',
                headers: {
                    Authorization: `Bearer ${this.state.token}`,
                    'Content-Type': 'application/json'
                }
            });
            let result = await res.json();
            this.setState({ activity: false });
            if (result.success === true) {
                this.props.Instructor_detail(result.data);
                this.props.navigation.navigate('instructorDetail', { type: 'PRIME', rating: item.average_rating });
            }
        }
        catch (error) {
            this.setState({ activity: false });
            if (error.message == 'Network request failed')
                Snackbar.show({
                    text: this.props.lang.internet_error,
                    duration: 4000
                })
        }
    }

    render() {
        return (
            <View style={baseStyles.appContainer}>
                <Activity show={this.state.activity} />
                <ScrollView>
                    <Item rounded>
                        <Input
                            placeholder={this.props.lang.search}
                            placeholderTextColor="#fff"
                            style={{ paddingLeft: padding.xl, color: colors.white }}
                            onFocus={() => this.props.navigation.navigate('search')}
                        />
                        <Icon name="ios-search" style={{ color: colors.white, paddingRight: padding.lg }} />
                    </Item>

                    <View style={baseStyles.marginTopLg}>
                        <ImageSlider
                            autoPlayWithInterval={5000}
                            images={this.props.slider}
                            customSlide={({ index, item, style, width }) => (
                                <View key={index} style={[style, Styles.customSlide]}>
                                    <Image source={{ uri: item.image_url }} style={Styles.customImage} />
                                </View>
                            )}
                        />
                    </View>

                    <View style={baseStyles.marginTopXl}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: margin.lg }}>
                            <Text style={Styles.heading}>{this.props.lang.celebrities}</Text>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('traneeList')}>
                                <Text style={Styles.text}>{this.props.lang.see_all}</Text>
                            </TouchableOpacity>
                        </View>
                        <FlatList
                            data={this.props.premium_instructor}
                            numColumns={1}
                            horizontal={true}
                            renderItem={({ item }) =>
                                <View style={[baseStyles.media]}>
                                    <TouchableOpacity style={{ marginRight: margin.lg }} onPress={() => {

                                        this.callApi(item);
                                    }} >
                                        <Thumbnail source={item.src} style={Styles.imgStyle} />
                                    </TouchableOpacity>
                                    <View style={[baseStyles.mediaBody]}>
                                        <Text style={Styles.nameStyle}>{item.name} </Text>
                                        <Text style={Styles.text}>{item.year_of_experience} Yrs of Experience</Text>
                                        <Text style={Styles.text}> {item.city},{item.state} </Text>
                                        <Text style={Styles.text}> {item.licence_type} Licence Trainer </Text>
                                        <View style={{ flexDirection: 'row' }} >
                                            {rating(parseInt(item.average_rating)).map(value => value)}
                                        </View>
                                    </View>
                                </View>
                            }
                        />
                    </View>


                    <View style={baseStyles.marginTopXl}>
                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginVertical: margin.lg }}>
                            <Text style={Styles.heading}>{this.props.lang.normal_trainer}</Text>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate('traneeList')}>
                                <Text style={Styles.text}>{this.props.lang.see_all}</Text>
                            </TouchableOpacity>
                        </View>
                        <FlatList
                            data={this.props.normal_instructor}
                            horizontal={true}
                            renderItem={({ item }) =>
                                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                                    <TouchableOpacity style={{ marginRight: margin.lg }} onPress={() => this.callApi(item)} >
                                        <Thumbnail source={item.src} style={Styles.imgStyle} />
                                    </TouchableOpacity>
                                    <Text style={Styles.nameStyle}>{item.name}</Text>
                                    <Text style={[baseStyles.whiteText, baseStyles.marginHorizontalMd]}>{item.experience}</Text>
                                </View>
                            }
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const Styles = {
    imgStyle: {
        height: 120,
        width: 120,
        borderRadius: radius.lg
    },
    imgRate: {
        height: 20,
        width: 120,
        marginRight: margin.lg
    },
    heading: {
        color: colors.white,
        fontSize: 25,
        fontWeight: 'bold'
    },
    text: {
        color: colors.white,
        fontSize: fonts.mld
    },
    nameStyle: {
        fontSize: fonts.xl,
        fontWeight: 'bold',
        color: colors.white
    },
    cardStyle: {
        borderRadius: radius.lg,
        borderWidth: 0,
        borderColor: null,
        marginTop: margin.lg
    },
    cardImgStyle: {
        height: 150,
        width: 300,
        flex: 1,
        resizeMode: 'cover',
        borderRadius: radius.lg
    },
    customImage: {
        height: 200,
        width: '100%',
        borderRadius: radius.xl,
        resizeMode: 'cover'
    }
}

const mapStateToProp = state => {
    return {
        lang: state.staticLang.userLanguage,
        slider: state.rootReducer.sliders,
        premium_instructor: state.rootReducer.premium_instructor.slice(0, 5),
        normal_instructor: state.rootReducer.normal_instructor.slice(0, 5),
    }
}

export default connect(mapStateToProp, { Instructor_detail })(MainDashboard);